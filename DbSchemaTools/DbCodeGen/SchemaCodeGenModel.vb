﻿<Serializable>
Public Class SchemaCodeGenModel
    Inherits BaseCodeGenModel

    Public Property Database As DbSchemaTools.DbInfo
    Public Property Tables As IList(Of DbSchemaTools.TableInfo)
    Public Property Helper As DbCodeGen.CodeGenHelper
End Class
