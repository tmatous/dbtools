﻿<Serializable>
Public Class BaseCodeGenModel
    Public Property OutputFormat As CodeGenerator.eOutputFormat
End Class

<Serializable>
Public Class BaseCodeGenModel(Of TData)
    Inherits BaseCodeGenModel

    Public Property Data As TData
End Class
