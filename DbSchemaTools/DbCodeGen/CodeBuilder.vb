﻿Public Class CodeBuilder
    Protected _mySb As New Text.StringBuilder
    Protected _indentLevel As Int32 = 0

    Public IndentString As String = vbTab


    Public Sub New(Optional pIndentLevel As Int32 = 0)
        _indentLevel = pIndentLevel
    End Sub

    Public Sub PushIndent(Optional pLevels As Int32 = 1)
        _indentLevel += pLevels
    End Sub

    Public Sub PopIndent(Optional pLevels As Int32 = 1)
        _indentLevel -= pLevels
    End Sub

    Public Function GetIndent() As Int32
        Return _indentLevel
    End Function

    Public Sub AppendLine(pToAdd As String)
        AppendIndent()
        _mySb.Append(pToAdd)
        _mySb.AppendLine()
    End Sub

    Public Sub AppendLineIndent(pToAdd As String)
        PushIndent()
        AppendLine(pToAdd)
        PopIndent()
    End Sub

    Public Sub AppendLineDeindent(pToAdd As String)
        PopIndent()
        AppendLine(pToAdd)
        PushIndent()
    End Sub

    Public Sub AppendLine()
        _mySb.AppendLine()
    End Sub

    Public Sub AppendNoIndent(pToAdd As String)
        _mySb.Append(pToAdd)
    End Sub

    Public Sub AppendLines(pToAdd As String)
        If (pToAdd.EndsWith(vbCrLf)) Then pToAdd = pToAdd.Substring(0, pToAdd.Length - 2)
        For Each ln In pToAdd.Split({vbCrLf}, StringSplitOptions.None)
            AppendLine(ln)
        Next
    End Sub

    Public Sub AppendFormatLine(pFormat As String, pArg1 As Object, Optional pArg2 As Object = Nothing, Optional pArg3 As Object = Nothing, Optional pArg4 As Object = Nothing, Optional pArg5 As Object = Nothing, Optional pArg6 As Object = Nothing)
        AppendIndent()
        _mySb.AppendFormat(pFormat, pArg1, pArg2, pArg3, pArg4, pArg5, pArg6)
        _mySb.AppendLine()
    End Sub

    Public Sub AppendFormatLineIndent(pFormat As String, pArg1 As Object, Optional pArg2 As Object = Nothing, Optional pArg3 As Object = Nothing, Optional pArg4 As Object = Nothing, Optional pArg5 As Object = Nothing, Optional pArg6 As Object = Nothing)
        PushIndent()
        AppendFormatLine(pFormat, pArg1, pArg2, pArg3, pArg4, pArg5, pArg6)
        PopIndent()
    End Sub

    Public Sub AppendFormatLineDeindent(pFormat As String, pArg1 As Object, Optional pArg2 As Object = Nothing, Optional pArg3 As Object = Nothing, Optional pArg4 As Object = Nothing, Optional pArg5 As Object = Nothing, Optional pArg6 As Object = Nothing)
        PopIndent()
        AppendFormatLine(pFormat, pArg1, pArg2, pArg3, pArg4, pArg5, pArg6)
        PushIndent()
    End Sub

    Public Sub AppendBuilder(pBuilder As CodeBuilder)
        Dim toAdd As String = pBuilder.ToString
        If (toAdd.EndsWith(vbCrLf)) Then toAdd = toAdd.Substring(0, toAdd.Length - 2)
        For Each ln In toAdd.Split({vbCrLf}, StringSplitOptions.None)
            AppendLine(ln)
        Next
    End Sub

    Public Overrides Function ToString() As String
        Return _mySb.ToString
    End Function

    Protected Sub AppendIndent()
        For x As Int32 = 1 To _indentLevel
            _mySb.Append(IndentString)
        Next
    End Sub



    'Not sure what this was built for...

    '#Region "Custom Code"

    '    Public Interface ICustomCodeGenerator
    '        Function Generate(pMasterGenerator As CodeGenerator, pTable As DbSchemaTools.TableInfo) As CodeBuilder
    '    End Interface


    '    Public Shared Function CreateGenerator(pCodeFile As String) As ICustomCodeGenerator
    '        Return CreateObjectFromCodeFile(Of ICustomCodeGenerator)(pCodeFile, "CustomCodeGenerator")
    '    End Function

    '    Protected Shared Function CreateObjectFromCodeFile(Of T)(pCodeFile As String, Optional pConcreteClassName As String = Nothing) As T
    '        If (Not IO.File.Exists(pCodeFile)) Then Return Nothing
    '        Dim ext = IO.Path.GetExtension(pCodeFile)
    '        If (Not CodeDom.Compiler.CodeDomProvider.IsDefinedExtension(ext)) Then Return Nothing
    '        Dim provider As CodeDom.Compiler.CodeDomProvider = CodeDom.Compiler.CodeDomProvider.CreateProvider(CodeDom.Compiler.CodeDomProvider.GetLanguageFromExtension(ext))

    '        Dim options As New CodeDom.Compiler.CompilerParameters()
    '        'options.ReferencedAssemblies.Add("System.dll")

    '        'NOTE: if the code file is untrusted, you should not give it access to all loaded assemblies
    '        For Each refAss As Reflection.Assembly In AppDomain.CurrentDomain.GetAssemblies()
    '            If (Not String.IsNullOrEmpty(refAss.Location)) Then options.ReferencedAssemblies.Add(refAss.Location)
    '        Next
    '        options.GenerateInMemory = True
    '        options.TreatWarningsAsErrors = True

    '        Dim compileOutput As CodeDom.Compiler.CompilerResults = provider.CompileAssemblyFromFile(options, pCodeFile)

    '        If (compileOutput.Errors.Count = 0) Then
    '            If (pConcreteClassName Is Nothing) Then pConcreteClassName = GetType(T).FullName
    '            Dim dynamicType As Type = compileOutput.CompiledAssembly.GetType(pConcreteClassName)
    '            If (dynamicType Is Nothing) Then Return Nothing
    '            Dim obj = System.Activator.CreateInstance(dynamicType)
    '            If (obj Is Nothing) Then Return Nothing
    '            Return DirectCast(obj, T)
    '        Else
    '            Dim erList As New Text.StringBuilder
    '            For Each er As CodeDom.Compiler.CompilerError In compileOutput.Errors
    '                erList.AppendLine(er.ErrorText)
    '            Next
    '            Throw New Exception(erList.ToString)
    '        End If

    '        Return Nothing
    '    End Function

    '#End Region





End Class
