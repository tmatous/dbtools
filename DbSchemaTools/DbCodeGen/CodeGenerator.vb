﻿Imports DbSchemaTools

Public MustInherit Class CodeGenerator

    Public Enum eOutputFormat
        NotSet
        VB
        CS
        HTM
        CSV
        TXT
        XML
        SQL
    End Enum


    Private Shared _host As Westwind.RazorHosting.RazorStringHostContainer = Nothing

    Public Shared Sub Init(Optional pLoadTypes As IEnumerable(Of Type) = Nothing)
        If (_host IsNot Nothing) Then Cleanup()

        _host = New Westwind.RazorHosting.RazorStringHostContainer()
        _host.AddAssemblyFromType(GetType(DbSchemaTools.DbInfo))
        _host.AddAssemblyFromType(GetType(DbCodeGen.CodeGenerator))
        _host.AddAssemblyFromType(GetType(Rational.DB.Database))
        _host.AddAssemblyFromType(GetType(System.Data.IDataReader))
        If (pLoadTypes IsNot Nothing) Then
            For Each typ In pLoadTypes
                _host.AddAssemblyFromType(typ)
            Next
        End If

        _host.Start()
    End Sub

    Public Shared Sub Cleanup()
        If (_host IsNot Nothing) Then
            _host.Stop()
            _host.Dispose()
            _host = Nothing
        End If
    End Sub

    Public Shared Function GetRazorHost() As Westwind.RazorHosting.RazorStringHostContainer
        If (_host Is Nothing) Then Init()
        Return _host
    End Function



    Public Shared Function RenderTemplateForSchema(pTemplateCode As String, pDb As DbSchemaTools.DbInfo, pTables As IEnumerable(Of DbSchemaTools.TableInfo)) As String
        Dim host = GetRazorHost()
        Dim fmt = DbCodeGen.CodeGenerator.eOutputFormat.NotSet
        GetTemplateOutputFormat(pTemplateCode, fmt)
        Dim helper = New DbCodeGen.CodeGenHelper(fmt)

        Dim model As New DbCodeGen.SchemaCodeGenModel With {.OutputFormat = fmt, .Database = pDb, .Tables = pTables.ToList(), .Helper = helper}
        Dim templateResult = host.RenderTemplate(pTemplateCode, model)
        If (templateResult Is Nothing) Then Throw New Exception(String.Format("Error rendering template: {0}", host.ErrorMessage))

        Return templateResult
    End Function


    Public Shared Function RenderTemplateForTable(pTemplateCode As String, pTable As DbSchemaTools.TableInfo) As String
        Dim host = GetRazorHost()
        Dim fmt = DbCodeGen.CodeGenerator.eOutputFormat.NotSet
        GetTemplateOutputFormat(pTemplateCode, fmt)
        Dim helper = New DbCodeGen.CodeGenHelper(fmt)

        Dim model As New DbCodeGen.TableCodeGenModel With {.OutputFormat = fmt, .Table = pTable, .Helper = helper}
        Dim templateResult = host.RenderTemplate(pTemplateCode, model)
        If (templateResult Is Nothing) Then Throw New Exception(String.Format("Error rendering template: {0}", host.ErrorMessage))

        Return templateResult
    End Function


    Public Shared Sub RenderTemplateForDataReader(pTemplateCode As String, pReader As IDataReader, pOutputFile As IO.TextWriter)
        Dim host = GetRazorHost()
        Dim fmt = DbCodeGen.CodeGenerator.eOutputFormat.NotSet
        GetTemplateOutputFormat(pTemplateCode, fmt)
        Dim helper = New DbCodeGen.CodeGenHelper(fmt)

        Dim model As New DbCodeGen.DataReaderCodeGenModel With {.OutputFormat = fmt, .Reader = pReader, .Helper = helper}
        Dim templateResult = host.RenderTemplate(pTemplateCode, model, pOutputFile)
        If (templateResult Is Nothing) Then Throw New Exception(String.Format("Error rendering template: {0}", host.ErrorMessage))
    End Sub


    Public Shared Sub RenderTemplateForDataRecord(pTemplateCode As String, pRecord As IDataRecord, pOutputFile As IO.TextWriter)
        Dim host = GetRazorHost()
        Dim fmt = DbCodeGen.CodeGenerator.eOutputFormat.NotSet
        GetTemplateOutputFormat(pTemplateCode, fmt)
        Dim helper = New DbCodeGen.CodeGenHelper(fmt)

        Dim model As New DbCodeGen.DataRecordCodeGenModel With {.OutputFormat = fmt, .Record = pRecord, .Helper = helper}
        Dim templateResult = host.RenderTemplate(pTemplateCode, model, pOutputFile)
        If (templateResult Is Nothing) Then Throw New Exception(String.Format("Error rendering template: {0}", host.ErrorMessage))
    End Sub


    Public Shared Function RenderTemplate(pTemplateCode As String, pModel As Object) As String
        If (pModel IsNot Nothing) Then Init({pModel.GetType()})
        Dim host = GetRazorHost()
        Dim templateResult = host.RenderTemplate(pTemplateCode, pModel)
        If (templateResult Is Nothing) Then Throw New Exception(String.Format("Error rendering template: {0}", host.ErrorMessage))

        Return templateResult
    End Function

    Public Shared Sub RenderTemplate(pTemplateCode As String, pModel As Object, pOutputFile As IO.TextWriter)
        If (pModel IsNot Nothing) Then Init({pModel.GetType()})
        Dim host = GetRazorHost()
        Dim templateResult = host.RenderTemplate(pTemplateCode, pModel, pOutputFile)
        If (templateResult Is Nothing) Then Throw New Exception(String.Format("Error rendering template: {0}", host.ErrorMessage))
    End Sub

    Public Shared Function GetTemplateOutputFormat(pTemplateCode As String) As DbCodeGen.CodeGenerator.eOutputFormat
        Dim fmt = DbCodeGen.CodeGenerator.eOutputFormat.NotSet
        GetTemplateOutputFormat(pTemplateCode, fmt)
        Return fmt
    End Function

    Private Shared Sub GetTemplateOutputFormat(ByRef pioTemplateCode As String, ByRef poOutputFormat As DbCodeGen.CodeGenerator.eOutputFormat)
        Dim fmt = ""

        Dim langRegex = New System.Text.RegularExpressions.Regex("@\*OutputFormat:(..+)\*@\s*")
        Dim mtch = langRegex.Match(pioTemplateCode)
        If (mtch.Success) Then
            fmt = mtch.Groups(1).Value
            pioTemplateCode = langRegex.Replace(pioTemplateCode, "", 1)
        End If

        [Enum].TryParse(Of DbCodeGen.CodeGenerator.eOutputFormat)(fmt, poOutputFormat)
    End Sub

End Class

