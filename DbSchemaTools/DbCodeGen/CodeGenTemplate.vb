﻿Imports Westwind.RazorHosting

Public Class CodeGenTemplate(Of TModel As {New, BaseCodeGenModel})
    Inherits Westwind.RazorHosting.RazorTemplateBase(Of TModel)

    Private Context As TModel = Nothing

    Public Overrides Sub InitializeTemplate(pModel As Object, Optional pConfig As Object = Nothing)
        Context = TryCast(pModel, TModel)
        Model = Context
    End Sub

    'the standard template HTML-encodes everything. This won't
    Public Overrides Sub Write(Optional value As Object = Nothing)
        If (value Is Nothing) Then Return
        If (Model.OutputFormat = CodeGenerator.eOutputFormat.HTM) Then
            'this escapes HTML unless the value is an instance of RawString or IHtmlString
            MyBase.Write(value)
        ElseIf (Model.OutputFormat = CodeGenerator.eOutputFormat.XML) Then
            'TODO: should this be a more proper XML encoding?
            MyBase.Write(value)
        Else
            WriteLiteral(value.ToString())
        End If
    End Sub


    Public Overrides Sub WriteTo(writer As IO.TextWriter, value As Object)
        If (value Is Nothing) Then Return
        If (Model.OutputFormat = CodeGenerator.eOutputFormat.HTM) Then
            'this escapes HTML unless the value is an instance of RawString or IHtmlString
            MyBase.WriteTo(writer, value)
        ElseIf (Model.OutputFormat = CodeGenerator.eOutputFormat.XML) Then
            'TODO: should this be a more proper XML encoding?
            MyBase.WriteTo(writer, value)
        Else
            WriteLiteral(value.ToString())
        End If
    End Sub

End Class
