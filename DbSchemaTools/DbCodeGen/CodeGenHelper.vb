﻿Imports DbSchemaTools

<Serializable>
Public Class CodeGenHelper


    Public Property IndentString As String = vbTab

    Protected _lang As CodeGenerator.eOutputFormat
    Protected _codeProvider As CodeDom.Compiler.CodeDomProvider

    Public ReadOnly Property CodeLanguage() As CodeGenerator.eOutputFormat
        Get
            Return _lang
        End Get
    End Property

    Public Sub New(pLanguage As CodeGenerator.eOutputFormat)
        _lang = pLanguage
        If (_lang = CodeGenerator.eOutputFormat.VB) Then
            _codeProvider = CodeDom.Compiler.CodeDomProvider.CreateProvider("VisualBasic")
        Else
            _codeProvider = CodeDom.Compiler.CodeDomProvider.CreateProvider("CSharp")
        End If
    End Sub

    Public Function GenerateReadTable(pTable As TableInfo, Optional pIndentLevel As Int32 = 0) As String
        If (Not {CodeGenerator.eOutputFormat.CS, CodeGenerator.eOutputFormat.VB}.Contains(_lang)) Then Throw New Exception("Code language not specified")

        Dim res As New System.Text.StringBuilder
        Dim indent As String = CreateIndentString(pIndentLevel)

        For Each col In pTable.Columns
            If (_lang = CodeGenerator.eOutputFormat.VB) Then
                res.AppendFormat("{0}obj.{1} = data.{3}(""{2}"")", indent, col.ClrName, col.Name, GetColumnReadFunction(col))
            Else
                res.AppendFormat("{0}obj.{1} = data.{3}(""{2}"");", indent, col.ClrName, col.Name, GetColumnReadFunction(col))
            End If
            res.AppendLine()
        Next

        Return res.ToString
    End Function

    Public Function GenerateWriteTable(pTable As TableInfo, Optional pIndentLevel As Int32 = 0) As String
        If (Not {CodeGenerator.eOutputFormat.CS, CodeGenerator.eOutputFormat.VB}.Contains(_lang)) Then Throw New Exception("Code language not specified")

        Dim res As New System.Text.StringBuilder
        Dim indent As String = CreateIndentString(pIndentLevel)

        For Each col In pTable.Columns
            If (_lang = CodeGenerator.eOutputFormat.VB) Then
                res.AppendFormat("{0}stmt.Parameters.{1}(""@{2}"", obj.{3})", indent, GetColumnParameterFunction(col), col.Name, col.ClrName)
            Else
                res.AppendFormat("{0}stmt.Parameters.{1}(""@{2}"", obj.{3});", indent, GetColumnParameterFunction(col), col.Name, col.ClrName)
            End If
            res.AppendLine()
        Next

        Return res.ToString
    End Function




    Public Function IsTableReadWrite(pTable As TableInfo) As Boolean
        If (pTable.TableType = TableInfo.eTableType.Table) Then Return True
        Return False
    End Function

    Public Function IsTableUsingIdentityKey(pTable As TableInfo) As Boolean
        If (pTable.TableType <> TableInfo.eTableType.Table) Then Return False
        Dim table = DirectCast(pTable, TableInfo)
        If ((table.PkColumns.Count = 1) AndAlso (table.PkColumns(0).DataClass = eDataClass.IntegerType) AndAlso (table.PkColumns(0).IsIdentity)) Then Return True
        Return False
    End Function

    Public Function IsColumnReadWrite(pCol As ColumnInfo) As Boolean
        If (Not (pCol.IsComputed OrElse pCol.IsIdentity OrElse pCol.IsRowVersion)) Then Return True
        Return False
    End Function


    Public Function GetClrTypeName(pCol As ColumnInfo) As String
        If (pCol.IsRowVersion) Then Return "UInt64"
        Dim res = ""
        If (pCol.ClrType.IsArray) Then
            If (Me._lang = CodeGenerator.eOutputFormat.VB) Then
                res = pCol.ClrType.Name.Replace("["c, "("c).Replace("]"c, ")"c)
            Else
                res = pCol.ClrType.Name
            End If
        Else
            res = pCol.ClrType.Name
        End If
        If (pCol.Nullable AndAlso pCol.ClrType.IsValueType) Then
            res += "?"
        End If

        Return res
    End Function

    Public Function GetDefaultValueStr(pCol As ColumnInfo) As String
        If (pCol.Nullable) Then
            Return If(Me._lang = CodeGenerator.eOutputFormat.VB, "Nothing", "null")
        ElseIf ((pCol.DataClass = eDataClass.StringType) AndAlso (Not pCol.Nullable)) Then
            Return "String.Empty"
        ElseIf (GetType(Boolean) = pCol.ClrType) Then
            Return If(Me._lang = CodeGenerator.eOutputFormat.VB, "False", "false")
        ElseIf (pCol.DataClass = eDataClass.IntegerType) Then
            Return "0"
        ElseIf (pCol.DataClass = eDataClass.FloatingPointType) Then
            Return "0.0"
        ElseIf (pCol.DataClass = eDataClass.DecimalType) Then
            Return If(Me._lang = CodeGenerator.eOutputFormat.VB, "0D", "0M")
        ElseIf (pCol.DataClass = eDataClass.DateTimeType) Then
            Return "DateTime.MinValue"
        Else
            Return If(Me._lang = CodeGenerator.eOutputFormat.VB, "Nothing", "null")
        End If
    End Function

    ''' <summary>
    ''' Get the parameter list for a function definition that accepts the key column(s) for the table (e.g. ReadByKey)
    ''' </summary>
    Public Function GetKeyFunctionDefParams(pTable As TableInfo) As String
        Dim keyFunctionDefParams = ""
        For Each pkcol In pTable.PkColumns
            If (keyFunctionDefParams.Length > 0) Then keyFunctionDefParams += ", "

            If (Me._lang = CodeGenerator.eOutputFormat.VB) Then
                keyFunctionDefParams += String.Format("{0} As {1}", EscapeIdentifier(pkcol.ClrName), GetClrTypeName(pkcol))
            Else
                keyFunctionDefParams += String.Format("{1} {0}", EscapeIdentifier(pkcol.ClrName), GetClrTypeName(pkcol))
            End If
        Next
        Return keyFunctionDefParams
    End Function

    ''' <summary>
    ''' Get the parameter list for a call to a function that accepts the key column(s) for the table (e.g. ReadByKey)
    ''' Should be used in conjunction with GetKeyFunctionDefParams
    ''' </summary>
    Public Function GetKeyFunctionCallParams(pTable As TableInfo) As String
        Dim keyFunctionCallParams = ""
        For Each pkcol In pTable.PkColumns
            If (keyFunctionCallParams.Length > 0) Then keyFunctionCallParams += ", "
            keyFunctionCallParams += String.Format("{0}", EscapeIdentifier(pkcol.ClrName))
        Next
        Return keyFunctionCallParams
    End Function

    ''' <summary>
    ''' Get the SQL where condition for targeting a row by key
    ''' </summary>
    Public Function GetKeySqlClause(pTable As TableInfo) As String
        Dim keySqlClause = ""
        For Each pkcol In pTable.PkColumns
            If (keySqlClause.Length > 0) Then
                keySqlClause += " AND "
            End If
            keySqlClause += String.Format("[{0}] = @{0}", pkcol.Name)
        Next
        Return keySqlClause
    End Function

    Public Function GetColumnReadFunction(pCol As ColumnInfo) As String
        If (pCol.IsRowVersion) Then Return "GetRowVersion"
        Dim funcName = ""
        Select Case pCol.ClrType
            Case GetType(String)
                funcName = "GetString"
            Case GetType(Byte)
                funcName = If(pCol.Nullable, "GetByteNullable", "GetByte")
            Case GetType(Int16)
                funcName = If(pCol.Nullable, "GetInt16Nullable", "GetInt16")
            Case GetType(Int32)
                funcName = If(pCol.Nullable, "GetInt32Nullable", "GetInt32")
            Case GetType(Int64)
                funcName = If(pCol.Nullable, "GetInt64Nullable", "GetInt64")
            Case GetType(Single)
                funcName = If(pCol.Nullable, "GetSingleNullable", "GetSingle")
            Case GetType(Double)
                funcName = If(pCol.Nullable, "GetDoubleNullable", "GetDouble")
            Case GetType(Decimal)
                funcName = If(pCol.Nullable, "GetDecimalNullable", "GetDecimal")
            Case GetType(Boolean)
                funcName = If(pCol.Nullable, "GetBooleanNullable", "GetBoolean")
            Case GetType(DateTime)
                funcName = If(pCol.Nullable, "GetDateTimeNullable", "GetDateTime")
            Case GetType(TimeSpan)
                funcName = If(pCol.Nullable, "GetTimeSpanNullable", "GetTimeSpan")
            Case GetType(Guid)
                funcName = If(pCol.Nullable, "GetGuidNullable", "GetGuid")
            Case GetType(Byte())
                funcName = "GetByteArray"
            Case Else
                funcName = "GetUnknown"
        End Select

        Return funcName
    End Function

    Public Function GetColumnParameterFunction(pCol As ColumnInfo) As String
        If (pCol.IsRowVersion) Then Return "AddRowVersion"
        Dim funcName = ""
        Select Case pCol.ClrType
            Case GetType(String)
                funcName = If(pCol.StringUnicode, "AddUString", "AddString")
            Case GetType(Int32), GetType(Int16), GetType(Byte)
                funcName = "AddInt32"
            Case GetType(Int64)
                funcName = "AddInt64"
            Case GetType(Single)
                funcName = "AddSingle"
            Case GetType(Double)
                funcName = "AddDouble"
            Case GetType(Decimal)
                funcName = "AddDecimal"
            Case GetType(Boolean)
                funcName = "AddBoolean"
            Case GetType(DateTime)
                funcName = "AddDateTime"
            Case GetType(TimeSpan)
                funcName = "AddTimeSpan"
            Case GetType(Guid)
                funcName = "AddGuid"
            Case GetType(Byte())
                funcName = "AddByteArray"
            Case Else
                funcName = "AddUnknown"
        End Select
        If (pCol.Nullable) Then funcName += "Nullable"

        Return funcName
    End Function

    Public Function EscapeIdentifier(pVarName As String) As String
        Return _codeProvider.CreateEscapedIdentifier(pVarName)
    End Function

    Protected Function CreateIndentString(pIndentLevel As Int32) As String
        Dim res = ""
        For x As Int32 = 1 To pIndentLevel
            res += Me.IndentString
        Next
        Return res
    End Function

    Public Function CreateCsvLine(pData As IEnumerable, Optional pDelimiter As String = ",") As String
        Dim line As New Text.StringBuilder()
        Dim delim = ""
        For Each fieldData In pData
            line.Append(delim)
            line.Append(FormatCsvData(fieldData))
            delim = pDelimiter
        Next

        Return line.ToString
    End Function

    Public Function FormatCsvData(pData As Object) As String
        If (TypeOf pData Is System.DBNull) Then
            Return String.Empty
        ElseIf (pData Is Nothing) Then
            Return String.Empty
        Else
            If (TypeOf pData Is Byte()) Then
                Return String.Format("""0x{0}""", ByteArrayToHexString(DirectCast(pData, Byte())))
            Else
                Return String.Format("""{0}""", pData.ToString().Replace("""", """"""))
            End If
        End If
    End Function

    Public Function ByteArrayToHexString(pData As Byte()) As String
        Return String.Concat("0x", BitConverter.ToString(pData).Replace("-", ""))
    End Function

    Public Function RawString(pData As Object) As Westwind.RazorHosting.RawString
        Dim dataStr = If(pData IsNot Nothing, pData.ToString, "")
        Return New Westwind.RazorHosting.RawString(dataStr)
    End Function

    Public Function FormatSqlData(pData As Object) As String
        If (TypeOf pData Is System.DBNull) Then
            Return "NULL"
        ElseIf (pData Is Nothing) Then
            Return "''"
        Else
            'TODO: add some type checking to create better SQL literals
            Return String.Format("'{0}'", pData.ToString().Replace("'", "''"))
        End If
    End Function

End Class

