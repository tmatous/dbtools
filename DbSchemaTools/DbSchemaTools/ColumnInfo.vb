﻿<Serializable>
Public Class ColumnInfo
    Public Property Table As TableInfo
    Public Property Name As String
    Public Property ClrName As String
    Public Property ClrType As Type
    Public Property SqlType As Data.DbType
    Public Property DataClass As eDataClass
    Public Property StringUnicode As Boolean
    Public Property StringLength As Int64
    Public Property DecimalPrecision As Byte
    Public Property DecimalScale As Byte
    Public Property ByteLength As Int64
    Public Property Nullable As Boolean
    Public Property DdlDataType As String
    Public Property InPrimaryKey As Boolean
    Public Property InForeignKey As Boolean
    Public Property IsComputed As Boolean
    Public Property IsIdentity As Boolean
    Public Property IsRowVersion As Boolean
    Public Property Summary As String

    Public Overrides Function ToString() As String
        Return String.Format("ColumnInfo: {0}", Me.Name)
    End Function

    Friend Shared Function GetClrType(pDbType As DbType) As Type
        Select Case pDbType
            Case DbType.AnsiString, DbType.AnsiStringFixedLength
                Return GetType(String)
            Case DbType.String, DbType.StringFixedLength
                Return GetType(String)
            Case DbType.Byte, DbType.SByte
                Return GetType(Byte)
            Case DbType.Int16, DbType.UInt16
                Return GetType(Int16)
            Case DbType.Int32, DbType.UInt32
                Return GetType(Int32)
            Case DbType.Int64, DbType.UInt64
                Return GetType(Int64)
            Case DbType.Single
                Return GetType(Single)
            Case DbType.Double
                Return GetType(Double)
            Case DbType.Currency, DbType.VarNumeric, DbType.Decimal
                Return GetType(Decimal)
            Case DbType.Boolean
                Return GetType(Boolean)
            Case DbType.Date, DbType.DateTime, DbType.DateTime2
                Return GetType(DateTime)
            Case DbType.DateTimeOffset
                Return GetType(DateTimeOffset)
            Case DbType.Time
                Return GetType(TimeSpan)
            Case DbType.Binary
                Return GetType(Byte())
            Case DbType.Guid
                Return GetType(Guid)
            Case DbType.Xml
                Return GetType(String)
            Case DbType.Object
                Return GetType(Object)
            Case Else
                Throw New NotImplementedException("Unknown type: " + pDbType.ToString)
        End Select
    End Function

    ''' <summary>
    ''' Assign the DataClass and related properties (unicode) to the column, using the SqlType on the column
    ''' </summary>
    ''' <param name="pioColInfo"></param>
    ''' <remarks></remarks>
    Friend Shared Sub AssignDataClassFromSqlType(pioColInfo As ColumnInfo)
        Select Case pioColInfo.SqlType
            Case DbType.AnsiString, DbType.AnsiStringFixedLength
                pioColInfo.DataClass = eDataClass.StringType
                pioColInfo.StringUnicode = False
            Case DbType.String, DbType.StringFixedLength
                pioColInfo.DataClass = eDataClass.StringType
                pioColInfo.StringUnicode = True
            Case DbType.Int16, DbType.Int32, DbType.Int64, DbType.Byte, DbType.SByte, DbType.UInt16, DbType.UInt32, DbType.UInt64
                pioColInfo.DataClass = eDataClass.IntegerType
            Case DbType.Double, DbType.Single
                pioColInfo.DataClass = eDataClass.FloatingPointType
            Case DbType.Currency, DbType.VarNumeric, DbType.Decimal
                pioColInfo.DataClass = eDataClass.DecimalType
            Case DbType.Boolean
                pioColInfo.DataClass = eDataClass.IntegerType
            Case DbType.Date, DbType.DateTime, DbType.DateTime2, DbType.DateTimeOffset, DbType.Time
                pioColInfo.DataClass = eDataClass.DateTimeType
            Case DbType.Binary
                pioColInfo.DataClass = eDataClass.BinaryType
            Case DbType.Guid
                pioColInfo.DataClass = eDataClass.GuidType
            Case DbType.Xml
                pioColInfo.DataClass = eDataClass.XmlType
            Case DbType.Object
                pioColInfo.DataClass = eDataClass.ObjectType
            Case Else
                Throw New NotImplementedException("Unknown type: " + pioColInfo.SqlType.ToString)
        End Select
    End Sub
End Class

Public Enum eDataClass
    NotSet
    StringType
    DateTimeType
    IntegerType
    FloatingPointType
    DecimalType
    BinaryType
    GuidType
    XmlType
    ObjectType
End Enum
