﻿Imports System.Xml
Imports System.Xml.XPath.Extensions
Imports System.Data.Metadata.Edm
Imports System.Data.Entity.Design
Imports System.Data.Mapping

Public Class EdmxSchemaLoader
    Implements ISchemaLoader

    Protected _edmxFilePath As String

    Public Sub New(pEdmxFilepath As String)
        _edmxFilePath = pEdmxFilepath
    End Sub

    Public Event OnError(sender As Object, e As SchemaLoaderErrorEventArgs) Implements ISchemaLoader.OnError

    Public Function Load() As DbInfo Implements ISchemaLoader.Load
        Dim res As New DbInfo

        Dim metadataWs As MetadataWorkspace = LoadMetadata(_edmxFilePath)
        Dim edmItems = DirectCast(metadataWs.GetItemCollection(DataSpace.CSpace), EdmItemCollection)
        Dim storeItems = DirectCast(metadataWs.GetItemCollection(DataSpace.SSpace), StoreItemCollection)
        Dim mapItems = DirectCast(metadataWs.GetItemCollection(DataSpace.CSSpace), StorageMappingItemCollection)
        Dim nameMappings = LoadNameMappings(_edmxFilePath)

        For Each loopEntity As EntityType In storeItems.GetItems(Of EntityType)().OrderBy(Function(e) e.Name)
            Dim edmEntity As EntityType = loopEntity

            Dim tbl As New TableInfo
            tbl.Name = edmEntity.Name
            Dim tblMap As TableMapping = Nothing
            If (nameMappings.ContainsKey(tbl.Name)) Then
                tblMap = nameMappings(tbl.Name)
                tbl.ClrName = tblMap.ClrName
            Else
                tblMap = Nothing
                tbl.ClrName = tbl.Name
            End If

            For Each loopEdmProperty As EdmProperty In edmEntity.Properties.Where(Function(p) TypeOf p.TypeUsage.EdmType Is PrimitiveType AndAlso p.DeclaringType Is edmEntity)
                Dim edmProperty As EdmProperty = loopEdmProperty
                Dim typ = DirectCast(edmProperty.TypeUsage.EdmType, PrimitiveType)

                Dim col As New ColumnInfo
                col.Table = tbl
                col.Name = edmProperty.Name
                If ((tblMap IsNot Nothing) AndAlso (tblMap.ColumnMappings.ContainsKey(col.Name))) Then
                    col.ClrName = tblMap.ColumnMappings(col.Name).ClrName
                Else
                    col.ClrName = col.Name
                End If
                col.Nullable = edmProperty.Nullable

                col.ClrType = typ.ClrEquivalentType
                If (typ.NamespaceName = "SqlServer") Then
                    col.SqlType = SqlServerSchemaLoader.GetDbTypeShared(typ.Name)
                    col.IsRowVersion = SqlServerSchemaLoader.GetIsRowVersion(typ.Name)
                Else
                    Throw New NotImplementedException("Unsupported DB type: " + typ.NamespaceName)
                End If
                ColumnInfo.AssignDataClassFromSqlType(col)
                If (col.DataClass = eDataClass.StringType) Then
                    col.StringLength = (From f In edmProperty.TypeUsage.Facets Where f.Name = "MaxLength" Select Convert.ToInt64(f.Value)).FirstOrDefault
                    col.StringUnicode = (From f In edmProperty.TypeUsage.Facets Where f.Name = "Unicode" Select Convert.ToBoolean(f.Value)).FirstOrDefault
                End If

                If (typ.NamespaceName = "SqlServer") Then
                    SqlServerSchemaLoader.AssignDdlDataType(col, typ.Name)
                Else
                    Throw New NotImplementedException("Unsupported DB type: " + typ.NamespaceName)
                End If



                col.InPrimaryKey = If(edmEntity.KeyMembers.Contains(edmProperty), True, False)
                col.InForeignKey = edmEntity.NavigationProperties.Any(Function(p) p.GetDependentProperties().Contains(edmProperty))
                col.IsIdentity = edmProperty.TypeUsage.Facets.Any(Function(f) TypeOf f.Value Is StoreGeneratedPattern AndAlso DirectCast(f.Value, StoreGeneratedPattern) = StoreGeneratedPattern.Identity)
                col.IsComputed = edmProperty.TypeUsage.Facets.Any(Function(f) TypeOf f.Value Is StoreGeneratedPattern AndAlso DirectCast(f.Value, StoreGeneratedPattern) = StoreGeneratedPattern.Computed)

                tbl.Columns.Add(col)
                If (col.InPrimaryKey) Then tbl.PkColumns.Add(col)
                If (col.IsIdentity) Then tbl.IdentityColumn = col
            Next

            res.Tables.Add(tbl)
        Next
        Return res
    End Function

#Region "Load Metadata"

    Protected Shared Function LoadMetadata(pEdmxPath As String) As MetadataWorkspace
        Dim res As New MetadataWorkspace()
        Dim eic = LoadEdmItemCollection(pEdmxPath)
        Dim sic = LoadStoreItemCollection(pEdmxPath)
        Dim mic = LoadMappingItemCollection(pEdmxPath, eic, sic)
        res.RegisterItemCollection(eic)
        res.RegisterItemCollection(sic)
        res.RegisterItemCollection(mic)

        Return res
    End Function

    Protected Shared Function LoadEdmItemCollection(pEdmxPath As String) As EdmItemCollection
        If String.IsNullOrEmpty(pEdmxPath) Then Throw New ArgumentException("sourcePath")

        Dim itemCollection As EdmItemCollection = Nothing
        Dim modelDoc As XElement = XElement.Load(pEdmxPath, LoadOptions.SetBaseUri Or LoadOptions.SetLineInfo)
        Dim rootElement As XElement = Nothing
        If (Not TryLoadRootElementFromEdmx(modelDoc, MetadataConstants.EDMX_NAMESPACE_V2, MetadataConstants.CSDL_NAMESPACE_V2, MetadataConstants.CSDL_EDMX_SECTION_NAME, MetadataConstants.CSDL_ROOT_ELEMENT_NAME, rootElement)) Then
            If (Not TryLoadRootElementFromEdmx(modelDoc, MetadataConstants.EDMX_NAMESPACE_V1, MetadataConstants.CSDL_NAMESPACE_V1, MetadataConstants.CSDL_EDMX_SECTION_NAME, MetadataConstants.CSDL_ROOT_ELEMENT_NAME, rootElement)) Then
                Throw New Exception("Unable to load root element")
            End If
        End If

        Dim readers As New List(Of XmlReader)()
        Try
            readers.Add(rootElement.CreateReader())
            Dim errors As IList(Of EdmSchemaError) = Nothing

            itemCollection = MetadataItemCollectionFactory.CreateEdmItemCollection(readers, errors)
            Dim errCount As Int32 = (From e In errors Where e.Severity = EdmSchemaErrorSeverity.Error).Count
            If (errCount > 0) Then
                Throw New Exception("Error loading model")
            End If
        Finally
            For Each reader As XmlReader In readers
                DirectCast(reader, IDisposable).Dispose()
            Next
        End Try
        Return itemCollection
    End Function

    Protected Shared Function LoadStoreItemCollection(pEdmxPath As String) As StoreItemCollection
        If String.IsNullOrEmpty(pEdmxPath) Then Throw New ArgumentException("sourcePath")

        Dim itemCollection As StoreItemCollection = Nothing
        Dim modelDoc As XElement = XElement.Load(pEdmxPath, LoadOptions.SetBaseUri Or LoadOptions.SetLineInfo)
        Dim rootElement As XElement = Nothing
        If (Not TryLoadRootElementFromEdmx(modelDoc, MetadataConstants.EDMX_NAMESPACE_V2, MetadataConstants.SSDL_NAMESPACE_V2, MetadataConstants.SSDL_EDMX_SECTION_NAME, MetadataConstants.SSDL_ROOT_ELEMENT_NAME, rootElement)) Then
            If (Not TryLoadRootElementFromEdmx(modelDoc, MetadataConstants.EDMX_NAMESPACE_V1, MetadataConstants.SSDL_NAMESPACE_V1, MetadataConstants.SSDL_EDMX_SECTION_NAME, MetadataConstants.SSDL_ROOT_ELEMENT_NAME, rootElement)) Then
                Throw New Exception("Unable to load root element")
            End If
        End If

        Dim readers As New List(Of XmlReader)()
        Try
            readers.Add(rootElement.CreateReader())
            Dim errors As IList(Of EdmSchemaError) = Nothing

            itemCollection = MetadataItemCollectionFactory.CreateStoreItemCollection(readers, errors)
            Dim errCount As Int32 = (From e In errors Where e.Severity = EdmSchemaErrorSeverity.Error).Count
            If (errCount > 0) Then
                Throw New Exception("Error loading model")
            End If
        Finally
            For Each reader As XmlReader In readers
                DirectCast(reader, IDisposable).Dispose()
            Next
        End Try
        Return itemCollection
    End Function

    Protected Shared Function LoadMappingItemCollection(pEdmxPath As String, pEdmCollection As EdmItemCollection, pStoreCollection As StoreItemCollection) As StorageMappingItemCollection
        If String.IsNullOrEmpty(pEdmxPath) Then Throw New ArgumentException("sourcePath")

        Dim itemCollection As StorageMappingItemCollection = Nothing
        Dim modelDoc As XElement = XElement.Load(pEdmxPath, LoadOptions.SetBaseUri Or LoadOptions.SetLineInfo)
        Dim rootElement As XElement = Nothing
        If (Not TryLoadRootElementFromEdmx(modelDoc, MetadataConstants.EDMX_NAMESPACE_V2, MetadataConstants.MSL_NAMESPACE_V2, MetadataConstants.MSL_EDMX_SECTION_NAME, MetadataConstants.MSL_ROOT_ELEMENT_NAME, rootElement)) Then
            If (Not TryLoadRootElementFromEdmx(modelDoc, MetadataConstants.EDMX_NAMESPACE_V1, MetadataConstants.MSL_NAMESPACE_V1, MetadataConstants.MSL_EDMX_SECTION_NAME, MetadataConstants.MSL_ROOT_ELEMENT_NAME, rootElement)) Then
                Throw New Exception("Unable to load root element")
            End If
        End If

        Dim readers As New List(Of XmlReader)()
        Try
            readers.Add(rootElement.CreateReader())
            Dim errors As IList(Of EdmSchemaError) = Nothing

            itemCollection = MetadataItemCollectionFactory.CreateStorageMappingItemCollection(pEdmCollection, pStoreCollection, readers, errors)
            Dim errCount As Int32 = (From e In errors Where e.Severity = EdmSchemaErrorSeverity.Error).Count
            If (errCount > 0) Then
                Throw New Exception("Error loading model")
            End If
        Finally
            For Each reader As XmlReader In readers
                DirectCast(reader, IDisposable).Dispose()
            Next
        End Try
        Return itemCollection
    End Function

    Protected Class TableMapping
        Public Property Name As String
        Public Property ClrName As String
        Public Property ColumnMappings As New Dictionary(Of String, ColumnMapping)
    End Class

    Protected Class ColumnMapping
        Public Property Name As String
        Public Property ClrName As String
    End Class

    Protected Shared Function LoadNameMappings(pEdmxPath As String) As Dictionary(Of String, TableMapping)
        If String.IsNullOrEmpty(pEdmxPath) Then Throw New ArgumentException("sourcePath")

        Dim modelDoc As XElement = XElement.Load(pEdmxPath, LoadOptions.SetBaseUri Or LoadOptions.SetLineInfo)
        Dim rootElement As XElement = Nothing
        Dim sectionNs As XNamespace = MetadataConstants.EDMX_NAMESPACE_V2
        If (Not TryLoadRootElementFromEdmx(modelDoc, MetadataConstants.EDMX_NAMESPACE_V2, MetadataConstants.MSL_NAMESPACE_V2, MetadataConstants.MSL_EDMX_SECTION_NAME, MetadataConstants.MSL_ROOT_ELEMENT_NAME, rootElement)) Then
            If (Not TryLoadRootElementFromEdmx(modelDoc, MetadataConstants.EDMX_NAMESPACE_V1, MetadataConstants.MSL_NAMESPACE_V1, MetadataConstants.MSL_EDMX_SECTION_NAME, MetadataConstants.MSL_ROOT_ELEMENT_NAME, rootElement)) Then
                Throw New Exception("Unable to load root element")
            End If
        End If

        '    <!-- C-S mapping content -->
        '    <edmx:Mappings>
        '      <Mapping Space="C-S" xmlns="http://schemas.microsoft.com/ado/2008/09/mapping/cs">
        '        <EntityContainerMapping StorageEntityContainer="TestDbModelStoreContainer" CdmEntityContainer="TestDbEntities">
        '          <EntitySetMapping Name="junk_es">
        '            <EntityTypeMapping TypeName="TestDbModel.junk_ent">
        '              <MappingFragment StoreEntitySet="junk">
        '                <ScalarProperty Name="id" ColumnName="id" />
        '                <ScalarProperty Name="junk_col_clr" ColumnName="junk" />
        '              </MappingFragment>
        '            </EntityTypeMapping>
        '          </EntitySetMapping>
        '        </EntityContainerMapping>
        '      </Mapping>
        '    </edmx:Mappings>

        Dim res As New Dictionary(Of String, TableMapping)
        For Each ent As XElement In (From e In rootElement.Descendants Where e.Name.LocalName = "EntityTypeMapping").ToList
            Dim tblMap As New TableMapping
            tblMap.ClrName = ent.Attribute("TypeName").Value
            If (tblMap.ClrName.Contains(".")) Then
                tblMap.ClrName = tblMap.ClrName.Substring(tblMap.ClrName.LastIndexOf("."c) + 1)
            End If
            Dim tbl As XElement = (From e In ent.Descendants Where e.Name.LocalName = "MappingFragment").FirstOrDefault
            tblMap.Name = tbl.Attribute("StoreEntitySet").Value
            For Each prop As XElement In (From e In tbl.Descendants Where e.Name.LocalName = "ScalarProperty").ToList
                Dim colMap As New ColumnMapping
                colMap.ClrName = prop.Attribute("Name").Value
                colMap.Name = prop.Attribute("ColumnName").Value
                tblMap.ColumnMappings.Add(colMap.Name, colMap)
            Next
            res.Add(tblMap.Name, tblMap)
        Next

        Return res
    End Function

    Protected Shared Function TryLoadRootElementFromEdmx(ByVal edmxDocument As XElement, ByVal edmxNamespace As String, ByVal sectionNamespace As String, ByVal sectionName As String, ByVal rootElementName As String, ByRef rootElement As XElement) As Boolean
        rootElement = Nothing

        Dim edmxNs As XNamespace = edmxNamespace
        Dim sectionNs As XNamespace = sectionNamespace

        Dim runtime As XElement = edmxDocument.Element(edmxNs + "Runtime")
        If runtime Is Nothing Then
            Return False
        End If

        Dim section As XElement = runtime.Element(edmxNs + sectionName)
        If section Is Nothing Then
            Return False
        End If

        rootElement = section.Element(sectionNs + rootElementName)
        Return rootElement IsNot Nothing
    End Function

    Protected Class MetadataConstants
        Private Sub New()
        End Sub
        Public Const EDMX_NAMESPACE_V1 As String = "http://schemas.microsoft.com/ado/2007/06/edmx"
        Public Const EDMX_NAMESPACE_V2 As String = "http://schemas.microsoft.com/ado/2008/10/edmx"


        Public Const CSDL_EXTENSION As String = ".csdl"
        Public Const CSDL_NAMESPACE_V1 As String = "http://schemas.microsoft.com/ado/2006/04/edm"
        Public Const CSDL_NAMESPACE_V2 As String = "http://schemas.microsoft.com/ado/2008/09/edm"
        Public Const CSDL_EDMX_SECTION_NAME As String = "ConceptualModels"
        Public Const CSDL_ROOT_ELEMENT_NAME As String = "Schema"
        Public Const EDM_ANNOTATION_09_02 As String = "http://schemas.microsoft.com/ado/2009/02/edm/annotation"

        Public Const SSDL_EXTENSION As String = ".ssdl"
        Public Const SSDL_NAMESPACE_V1 As String = "http://schemas.microsoft.com/ado/2006/04/edm/ssdl"
        Public Const SSDL_NAMESPACE_V2 As String = "http://schemas.microsoft.com/ado/2009/02/edm/ssdl"
        Public Const SSDL_EDMX_SECTION_NAME As String = "StorageModels"
        Public Const SSDL_ROOT_ELEMENT_NAME As String = "Schema"

        Public Const MSL_EXTENSION As String = ".msl"
        Public Const MSL_NAMESPACE_V1 As String = "urn:schemas-microsoft-com:windows:storage:mapping:CS"
        Public Const MSL_NAMESPACE_V2 As String = "http://schemas.microsoft.com/ado/2008/09/mapping/cs"
        Public Const MSL_EDMX_SECTION_NAME As String = "Mappings"
        Public Const MSL_ROOT_ELEMENT_NAME As String = "Mapping"
    End Class

#End Region

End Class
