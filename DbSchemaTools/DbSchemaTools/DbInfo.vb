﻿<Serializable>
Public Class DbInfo

    Public Property Tables As IList(Of TableInfo) = New List(Of TableInfo)
    Public Property Views As IList(Of TableInfo) = New List(Of TableInfo)
    Public Property TableFunctions As IList(Of TableInfo) = New List(Of TableInfo)

End Class
