﻿Public Class SqlServerSchemaLoader
    Implements ISchemaLoader

    Protected _connInfo As Rational.DB.DbConnectionInfo

    Public Sub New(pConnInfo As Rational.DB.DbConnectionInfo)
        _connInfo = pConnInfo
    End Sub

    Public Event OnError(sender As Object, e As SchemaLoaderErrorEventArgs) Implements ISchemaLoader.OnError

    Public Function Load() As DbInfo Implements ISchemaLoader.Load
        Dim res As New DbInfo

        Dim dtTables As DataTable
        Dim dtPriKeys As DataTable
        Dim dtForKeys As DataTable
        Dim dtAllCols As DataTable
        Dim db As New Rational.DB.Database(_connInfo)

        'load tables
        Dim sqlTables As New Rational.DB.DbStatement("SELECT IST.TABLE_NAME, IST.TABLE_SCHEMA, IST.TABLE_TYPE FROM INFORMATION_SCHEMA.TABLES IST ORDER BY IST.TABLE_NAME")
        dtTables = db.SelectDataTable(sqlTables)

        'load columns
        Dim sqlCols As New Rational.DB.DbStatement("SELECT ISC.TABLE_NAME, ISC.TABLE_SCHEMA, ISC.COLUMN_NAME, ISC.IS_NULLABLE, ISC.DATA_TYPE, (CASE COLUMNPROPERTY(object_id(ISC.TABLE_SCHEMA+'.'+ISC.TABLE_NAME), ISC.COLUMN_NAME, 'IsIdentity') WHEN 1 THEN 'YES' ELSE 'NO' END) AS IS_IDENTITY, (CASE COLUMNPROPERTY(object_id(ISC.TABLE_SCHEMA+'.'+ISC.TABLE_NAME), ISC.COLUMN_NAME, 'IsComputed') WHEN 1 THEN 'YES' ELSE 'NO' END) AS IS_COMPUTED, ISC.CHARACTER_MAXIMUM_LENGTH, ISC.CHARACTER_OCTET_LENGTH, ISC.ORDINAL_POSITION FROM INFORMATION_SCHEMA.COLUMNS ISC ORDER BY ISC.TABLE_NAME, ISC.ORDINAL_POSITION")
        dtAllCols = db.SelectDataTable(sqlCols)

        'load keys
        Dim sqlPriKeys = New Rational.DB.DbStatement("SELECT TC.TABLE_SCHEMA, TC.TABLE_NAME, TC.CONSTRAINT_NAME, KU.COLUMN_NAME, KU.ORDINAL_POSITION FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS TC INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE KU ON KU.CONSTRAINT_NAME = TC.CONSTRAINT_NAME WHERE TC.CONSTRAINT_TYPE = 'PRIMARY KEY' ORDER BY TC.TABLE_NAME, KU.ORDINAL_POSITION")
        dtPriKeys = db.SelectDataTable(sqlPriKeys)
        Dim sqlForKeys = New Rational.DB.DbStatement("SELECT SCHEMA_NAME(fk.schema_id) as TABLE_SCHEMA, OBJECT_NAME(fk.parent_object_id) AS TABLE_NAME, fk.name AS CONSTRAINT_NAME, COL_NAME(fc.parent_object_id, fc.parent_column_id) AS COLUMN_NAME, OBJECT_NAME(fk.referenced_object_id) AS REFERENCED_TABLE_NAME, COL_NAME(fc.referenced_object_id, fc.referenced_column_id) AS REFERENCED_COLUMN_NAME FROM sys.foreign_keys AS fk INNER JOIN sys.foreign_key_columns AS fc ON fc.constraint_object_id = fk.object_id")
        dtForKeys = db.SelectDataTable(sqlForKeys)

        'TODO: load procs, table functions
        'sqlTables.Statement.AppendRaw(" SELECT IST.ROUTINE_NAME AS TABLE_NAME, IST.ROUTINE_SCHEMA AS TABLE_SCHEMA, 'TABLE FUNCTION' AS TABLE_TYPE FROM INFORMATION_SCHEMA.ROUTINES IST WHERE IST.ROUTINE_TYPE = 'FUNCTION' AND IST.DATA_TYPE = 'TABLE'")
        'sqlCols.Statement.AppendRaw(" SELECT ISC.TABLE_NAME, ISC.TABLE_SCHEMA, ISC.COLUMN_NAME, ISC.IS_NULLABLE, ISC.DATA_TYPE, (CASE COLUMNPROPERTY(object_id(ISC.TABLE_SCHEMA+'.'+ISC.TABLE_NAME), ISC.COLUMN_NAME, 'IsIdentity') WHEN 1 THEN 'YES' ELSE 'NO' END) AS IS_IDENTITY, (CASE COLUMNPROPERTY(object_id(ISC.TABLE_SCHEMA+'.'+ISC.TABLE_NAME), ISC.COLUMN_NAME, 'IsComputed') WHEN 1 THEN 'YES' ELSE 'NO' END) AS IS_COMPUTED, ISC.CHARACTER_MAXIMUM_LENGTH, ISC.CHARACTER_OCTET_LENGTH, ISC.ORDINAL_POSITION FROM INFORMATION_SCHEMA.ROUTINE_COLUMNS ISC")

        Dim allColsByName = New Dictionary(Of String, ColumnInfo)
        For Each tblRow As DataRow In dtTables.Rows
            Dim tblType = tblRow.Item("TABLE_TYPE").ToString
            Dim tbl As New TableInfo
            Select Case tblType
                Case "BASE TABLE"
                    tbl.TableType = TableInfo.eTableType.Table
                    res.Tables.Add(tbl)
                Case "VIEW"
                    tbl.TableType = TableInfo.eTableType.View
                    res.Views.Add(tbl)
                    'Case "TABLE FUNCTION"
                    '   tbl.TableType = TableInfo.eTableType.TableFunction
                    '  res.TableFunctions.Add(tbl)
                Case Else : Continue For
            End Select

            tbl.Name = tblRow.Item("TABLE_NAME").ToString
            tbl.ClrName = tbl.Name
            tbl.SchemaName = tblRow.Item("TABLE_SCHEMA").ToString

            Dim pks As New List(Of String)
            For Each keyRow As DataRow In dtPriKeys.Select(String.Format("TABLE_NAME = '{0}' AND TABLE_SCHEMA = '{1}'", tbl.Name, tbl.SchemaName))
                pks.Add(keyRow.Item("COLUMN_NAME").ToString)
            Next


            For Each colRow As DataRow In dtAllCols.Select(String.Format("TABLE_NAME = '{0}' AND TABLE_SCHEMA = '{1}'", tbl.Name, tbl.SchemaName))
                Dim col As New ColumnInfo

                col.Table = tbl
                col.Name = colRow("COLUMN_NAME").ToString
                col.ClrName = col.Name
                col.Nullable = If((colRow("IS_NULLABLE").ToString.ToUpper = "YES"), True, False)

                col.SqlType = GetDbType(colRow("DATA_TYPE").ToString)
                col.ClrType = ColumnInfo.GetClrType(col.SqlType)
                ColumnInfo.AssignDataClassFromSqlType(col)
                If (col.DataClass = eDataClass.StringType) Then
                    col.StringLength = Convert.ToInt64(colRow("CHARACTER_MAXIMUM_LENGTH"))
                    col.ByteLength = Convert.ToInt32(colRow("CHARACTER_OCTET_LENGTH"))
                End If
                If (col.DataClass = eDataClass.BinaryType) Then
                    Dim octLength = colRow("CHARACTER_OCTET_LENGTH")
                    If (octLength IsNot DBNull.Value) Then col.ByteLength = Convert.ToInt32(octLength)
                End If

                AssignDdlDataType(col, colRow("DATA_TYPE").ToString)

                col.InPrimaryKey = If(pks.Contains(col.Name), True, False)
                col.InForeignKey = False
                col.IsIdentity = If((colRow("IS_IDENTITY").ToString.ToUpper = "YES"), True, False)
                col.IsRowVersion = GetIsRowVersion(colRow("DATA_TYPE").ToString)
                col.IsComputed = If((colRow("IS_COMPUTED").ToString.ToUpper = "YES"), True, False)

                tbl.Columns.Add(col)
                If (col.InPrimaryKey) Then tbl.PkColumns.Add(col)
                If (col.IsIdentity) Then tbl.IdentityColumn = col
                allColsByName.Add(String.Format("{0}.{1}.{2}", tbl.SchemaName, tbl.Name, col.Name), col)
            Next

        Next

        'assign FKs
        Dim allFks = New Dictionary(Of String, ForeignKeyInfo)
        For Each keyRow As DataRow In dtForKeys.Rows
            Dim colKey = String.Format("{0}.{1}.{2}", keyRow.Item("TABLE_SCHEMA"), keyRow.Item("TABLE_NAME"), keyRow.Item("COLUMN_NAME"))
            Dim colRefKey = String.Format("{0}.{1}.{2}", keyRow.Item("TABLE_SCHEMA"), keyRow.Item("REFERENCED_TABLE_NAME"), keyRow.Item("REFERENCED_COLUMN_NAME"))
            Dim fkname = keyRow.Item("CONSTRAINT_NAME").ToString
            Dim fk As ForeignKeyInfo
            If (allFks.ContainsKey(fkname)) Then
                fk = allFks.Item(fkname)
            Else
                fk = New ForeignKeyInfo()
                fk.Name = fkname
                allFks.Add(fkname, fk)
            End If
            Dim fkcol = allColsByName(colKey)
            Dim fkcolRef As ColumnInfo = Nothing
            If (allColsByName.ContainsKey(colRefKey)) Then
                fkcolRef = allColsByName.Item(colRefKey)
                If (Not fkcolRef.Table.ReferenceKeys.Contains(fk)) Then fkcolRef.Table.ReferenceKeys.Add(fk)
            End If
            If (Not fkcol.Table.ForeignKeys.Contains(fk)) Then fkcol.Table.ForeignKeys.Add(fk)
            fkcol.InForeignKey = True
            fk.Columns.Add(New ForeignKeyColumnInfo With {.Column = fkcol, .ReferencedColumn = fkcolRef})
        Next

        Return res
    End Function



    'others: sql_variant,timestamp,sysname,hierarchyid,geometry,geography
    'SELECT xtype, name FROM systypes ORDER BY xType
    Protected Function GetDbType(pSqlType As String) As DbType
        pSqlType = pSqlType.ToLower()
        Select Case pSqlType

            'integer (exact numerics)
            Case "int"
                Return DbType.Int32
            Case "smallint"
                Return DbType.Int16
            Case "tinyint"
                Return DbType.Byte
            Case "bigint"
                Return DbType.Int64

                'boolean (exact numerics)
            Case "bit"
                Return DbType.Boolean

                'floating point (exact numerics)
            Case "decimal", "numeric"
                Return DbType.Decimal
            Case "money", "smallmoney"
                Return DbType.Currency

                'floating point (approximate numerics)
            Case "real"
                Return DbType.Single
            Case "float"
                Return DbType.Double

                'strings (non unicode)
            Case "char"
                Return DbType.AnsiStringFixedLength
            Case "varchar", "varchar(max)", "text"
                Return DbType.AnsiString

                'unicode strings
            Case "nchar"
                Return DbType.StringFixedLength
            Case "nvarchar", "nvarchar(max)", "ntext"
                Return DbType.String

                'date and time
            Case "date"
                Return DbType.Date
            Case "time"
                Return DbType.Time
            Case "datetime", "smalldatetime", "datetime2"
                Return DbType.DateTime
            Case "datetimeoffset"
                Return DbType.DateTimeOffset

                'binary
            Case "binary", "varbinary", "varbinary(max)", "image"
                Return DbType.Binary

                'guid
            Case "uniqueidentifier"
                Return DbType.Guid

                'rowversion
            Case "rowversion", "timestamp"
                Return DbType.Binary

            Case "xml"
                Return DbType.Xml

                'other?
        End Select

        'no value here, raise an error event
        RaiseEvent OnError(Me, New SchemaLoaderErrorEventArgs With {.ErrorType = SchemaLoaderErrorEventArgs.eErrorType.UnsupportedType, .Message = "Unsupported type: " + pSqlType})

        Return DbType.Object
    End Function

    Friend Shared Function GetDbTypeShared(pSqlType As String) As DbType
        Dim inst = New SqlServerSchemaLoader(Nothing)
        Return inst.GetDbType(pSqlType)
    End Function

    ''' <summary>
    ''' Assign the DdlDataType property using the specified SQL type as well as size properties on the column already
    ''' </summary>
    ''' <param name="pioColInfo"></param>
    ''' <param name="pSqlType"></param>
    ''' <remarks></remarks>
    Friend Shared Sub AssignDdlDataType(pioColInfo As ColumnInfo, pSqlType As String)
        Dim typeArgs = ""
        Select Case pSqlType.ToLower
            Case "char", "nchar", "varchar", "nvarchar"
                typeArgs = String.Format("({0})", If(pioColInfo.StringLength = -1, "max", pioColInfo.StringLength.ToString))
            Case "binary", "varbinary"
                typeArgs = String.Format("({0})", If(pioColInfo.ByteLength = -1, "max", pioColInfo.ByteLength.ToString))
            Case Else
                typeArgs = ""
        End Select

        pioColInfo.DdlDataType = String.Format("{0}{1}", pSqlType, typeArgs)
    End Sub

    Friend Shared Function GetIsRowVersion(pSqlType As String) As Boolean
        If ((pSqlType = "rowversion") OrElse (pSqlType = "timestamp")) Then
            Return True
        Else
            Return False
        End If
    End Function

End Class
