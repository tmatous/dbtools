﻿Public Interface ISchemaLoader

    Event OnError(sender As Object, e As SchemaLoaderErrorEventArgs)
    Function Load() As DbInfo

End Interface

Public Class SchemaLoaderErrorEventArgs
    Public ErrorType As eErrorType
    Public Message As String

    Public Enum eErrorType
        UnsupportedType = 1
    End Enum

End Class
