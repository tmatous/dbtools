﻿Public Class OracleSchemaLoader
    Implements ISchemaLoader

    Protected _connInfo As Rational.DB.DbConnectionInfo

    Public Sub New(pConnInfo As Rational.DB.DbConnectionInfo)
        _connInfo = pConnInfo
    End Sub

    Public Event OnError(sender As Object, e As SchemaLoaderErrorEventArgs) Implements ISchemaLoader.OnError

    Public Function Load() As DbInfo Implements ISchemaLoader.Load
        Dim res As New DbInfo

        Dim dtTables As DataTable
        Dim dtAllCols As DataTable
        Dim dtPriKeys As DataTable
        Dim dtForKeys As DataTable
        Dim db As New Rational.DB.Database(_connInfo)
        Dim fctry = _connInfo.GetFactory()

        Using conn = fctry.CreateConnection()
            conn.ConnectionString = _connInfo.ConnectionString
            conn.Open()

            dtTables = conn.GetSchema("Tables")
            dtAllCols = conn.GetSchema("Columns")

            'load keys
            Dim sqlPriKeys = New Rational.DB.DbStatement("select pks.owner, pkcs.table_name, pkcs.column_name, pkcs.position from user_constraints pks left join user_cons_columns pkcs on pks.table_name = pkcs.table_name and pks.constraint_name = pkcs.constraint_name where pks.constraint_type = 'P' order by pkcs.table_name, pkcs.position")
            dtPriKeys = db.SelectDataTable(sqlPriKeys)

            Dim sqlForKeys = New Rational.DB.DbStatement("select fkcs.owner AS TABLE_SCHEMA, fkcs.constraint_name, fkcs.table_name, fkcs.column_name, fkcs.position, fkrcs.table_name as referenced_table_name, fkrcs.column_name as referenced_column_name from user_constraints fks left join user_cons_columns fkcs on fkcs.table_name = fks.table_name and fkcs.constraint_name = fks.constraint_name left join user_cons_columns fkrcs on fkrcs.constraint_name = fks.r_constraint_name and fkrcs.position = fkcs.position where fks.constraint_type = 'R' order by fkcs.table_name, fkcs.position")
            dtForKeys = db.SelectDataTable(sqlForKeys)
        End Using

        Dim allColsByName = New Dictionary(Of String, ColumnInfo)
        For Each tblRow As DataRow In dtTables.Rows
            Dim tblType = tblRow.Item("TYPE").ToString
            If (tblType.Equals("SYSTEM", StringComparison.OrdinalIgnoreCase)) Then Continue For

            Dim tbl As New TableInfo
            tbl.TableType = TableInfo.eTableType.Table
            res.Tables.Add(tbl)

            tbl.Name = tblRow.Item("TABLE_NAME").ToString
            tbl.ClrName = tbl.Name
            tbl.SchemaName = tblRow.Item("OWNER").ToString

            Dim pks As New List(Of String)
            For Each keyRow As DataRow In dtPriKeys.Select(String.Format("TABLE_NAME = '{0}' AND OWNER = '{1}'", tbl.Name, tbl.SchemaName))
                pks.Add(keyRow.Item("COLUMN_NAME").ToString)
            Next


            'TODO: load procs and views


            For Each colRow As DataRow In dtAllCols.Select(String.Format("TABLE_NAME = '{0}' AND OWNER = '{1}'", tbl.Name, tbl.SchemaName))
                Dim col As New ColumnInfo

                col.Table = tbl
                col.Name = colRow("COLUMN_NAME").ToString
                col.ClrName = col.Name
                col.Nullable = If((colRow("NULLABLE").ToString.ToUpper = "Y"), True, False)

                col.SqlType = GetDbType(colRow("DATATYPE").ToString)
                col.ClrType = ColumnInfo.GetClrType(col.SqlType)
                col.ByteLength = Convert.ToInt32(colRow("LENGTH"))
                ColumnInfo.AssignDataClassFromSqlType(col)
                If (col.DataClass = eDataClass.StringType) Then
                    If (dtAllCols.Columns.Contains("LENGTHINCHARS")) Then
                        col.StringLength = Convert.ToInt64(colRow("LENGTHINCHARS"))
                    ElseIf (col.StringUnicode) Then
                        col.StringLength = col.ByteLength \ 2
                    Else
                        col.StringLength = col.ByteLength
                    End If
                End If

                AssignDdlDataType(col, colRow("DATATYPE").ToString)

                col.InPrimaryKey = If(pks.Contains(col.Name), True, False)
                col.InForeignKey = False
                col.IsIdentity = False
                col.IsRowVersion = False
                col.IsComputed = False

                tbl.Columns.Add(col)
                If (col.InPrimaryKey) Then tbl.PkColumns.Add(col)
                If (col.IsIdentity) Then tbl.IdentityColumn = col
                allColsByName.Add(String.Format("{0}.{1}.{2}", tbl.SchemaName, tbl.Name, col.Name), col)
            Next
        Next


        'assign FKs
        Dim allFks = New Dictionary(Of String, ForeignKeyInfo)
        For Each keyRow As DataRow In dtForKeys.Rows
            Dim colKey = String.Format("{0}.{1}.{2}", keyRow.Item("TABLE_SCHEMA"), keyRow.Item("TABLE_NAME"), keyRow.Item("COLUMN_NAME"))
            Dim colRefKey = String.Format("{0}.{1}.{2}", keyRow.Item("TABLE_SCHEMA"), keyRow.Item("REFERENCED_TABLE_NAME"), keyRow.Item("REFERENCED_COLUMN_NAME"))
            Dim fkname = keyRow.Item("CONSTRAINT_NAME").ToString
            Dim fk As ForeignKeyInfo
            If (allFks.ContainsKey(fkname)) Then
                fk = allFks.Item(fkname)
            Else
                fk = New ForeignKeyInfo()
                fk.Name = fkname
                allFks.Add(fkname, fk)
            End If
            Dim fkcol = allColsByName(colKey)
            Dim fkcolRef As ColumnInfo = Nothing
            If (allColsByName.ContainsKey(colRefKey)) Then
                fkcolRef = allColsByName.Item(colRefKey)
                If (Not fkcolRef.Table.ReferenceKeys.Contains(fk)) Then fkcolRef.Table.ReferenceKeys.Add(fk)
            End If
            If (Not fkcol.Table.ForeignKeys.Contains(fk)) Then fkcol.Table.ForeignKeys.Add(fk)
            fkcol.InForeignKey = True
            fk.Columns.Add(New ForeignKeyColumnInfo With {.Column = fkcol, .ReferencedColumn = fkcolRef})
        Next

        Return res
    End Function


    '"BFILE","BINARY_DOUBLE","BINARY_FLOAT","BLOB","CHAR","CLOB","DATE","FLOAT","INTERVAL DAY TO SECOND","INTERVAL YEAR TO MONTH","LONG","LONG RAW","NCHAR","NCLOB","NUMBER","NVARCHAR2","RAW","TIMESTAMP","TIMESTAMP WITH LOCAL TIME ZONE","TIMESTAMP WITH TIME ZONE","VARCHAR2","XMLTYPE","ROWID"
    Protected Function GetDbType(pSqlType As String) As DbType
        pSqlType = pSqlType.ToUpper()
        Select Case pSqlType
            Case "BFILE"
                Return DbType.Binary
            Case "BINARY_DOUBLE"
                Return DbType.Double
            Case "BINARY_FLOAT"
                Return DbType.Single
            Case "BLOB"
                Return DbType.Binary
            Case "CHAR"
                Return DbType.AnsiStringFixedLength
            Case "CLOB"
                Return DbType.AnsiString
            Case "DATE"
                Return DbType.DateTime
            Case "FLOAT"
                Return DbType.Decimal
            Case "INTERVAL DAY TO SECOND"
                Return DbType.Time
            Case "INTERVAL YEAR TO MONTH"
                Return DbType.Int32
            Case "LONG"
                Return DbType.String
            Case "LONG RAW"
                Return DbType.Binary
            Case "NCHAR"
                Return DbType.StringFixedLength
            Case "NCLOB"
                Return DbType.String
            Case "NUMBER"
                Return DbType.Decimal
            Case "NVARCHAR2"
                Return DbType.String
            Case "RAW"
                Return DbType.Binary
            Case "TIMESTAMP"
                Return DbType.DateTime
            Case "TIMESTAMP WITH LOCAL TIME ZONE"
                Return DbType.DateTime
            Case "TIMESTAMP WITH TIME ZONE"
                Return DbType.DateTime
            Case "VARCHAR2"
                Return DbType.AnsiString
            Case "XMLTYPE"
                Return DbType.Xml
            Case "ROWID"
                Return DbType.Guid
            Case "ANYDATA"
                Return DbType.Object
        End Select

        'nonstandard, "TIMESTAMP(6)"
        If (pSqlType.StartsWith("TIMESTAMP")) Then Return DbType.DateTime

        'nonstandard, "INTERVAL YEAR [(year_precision)] TO MONTH"
        If (pSqlType.StartsWith("INTERVAL YEAR")) Then Return DbType.Int32

        'nonstandard, "INTERVAL DAY [(day_precision)] TO SECOND [(fractional_seconds)]"
        If (pSqlType.StartsWith("INTERVAL DAY")) Then Return DbType.Time

        'no value here, raise an error event
        RaiseEvent OnError(Me, New SchemaLoaderErrorEventArgs With {.ErrorType = SchemaLoaderErrorEventArgs.eErrorType.UnsupportedType, .Message = "Unsupported type: " + pSqlType})

        Return DbType.Object
    End Function

    ''' <summary>
    ''' Assign the DdlDataType property using the specified SQL type as well as size properties on the column already
    ''' </summary>
    ''' <param name="pioColInfo"></param>
    ''' <param name="pSqlType"></param>
    ''' <remarks></remarks>
    Protected Sub AssignDdlDataType(pioColInfo As ColumnInfo, pSqlType As String)
        Dim typeArgs = ""
        If (pioColInfo.DataClass = eDataClass.StringType) Then
            typeArgs = String.Format("({0})", pioColInfo.StringLength.ToString)
        End If

        pioColInfo.DdlDataType = String.Format("{0}{1}", pSqlType, typeArgs)
    End Sub

End Class
