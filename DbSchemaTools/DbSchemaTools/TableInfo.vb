﻿<Serializable>
Public Class TableInfo

    Public Enum eTableType
        Table
        View
        TableFunction
    End Enum

    Public Property TableType As eTableType
    Public Property Name As String
    Public Property ClrName As String
    Public Property SchemaName As String
    Public Property Columns As IList(Of ColumnInfo) = New List(Of ColumnInfo)
    Public Property Summary As String

    Public Property PkColumns As IList(Of ColumnInfo) = New List(Of ColumnInfo)
    Public Property IdentityColumn As ColumnInfo
    Public Property RowVersionColumn As ColumnInfo

    Public Property ForeignKeys As IList(Of ForeignKeyInfo) = New List(Of ForeignKeyInfo)
    Public Property ReferenceKeys As IList(Of ForeignKeyInfo) = New List(Of ForeignKeyInfo)

    Public Overridable ReadOnly Property SqlFields() As String
        Get
            GenSql()
            Return Me._sqlAllFields
        End Get
    End Property

    Public Overridable ReadOnly Property SqlSelect() As String
        Get
            GenSql()
            If (Me.TableType = eTableType.TableFunction) Then
                Return String.Format("SELECT {1} FROM [{0}]() ", Me.Name, Me._sqlAllFields)
            Else
                Return String.Format("SELECT {1} FROM [{0}] ", Me.Name, Me._sqlAllFields)
            End If
        End Get
    End Property

    Public ReadOnly Property SqlInsert() As String
        Get
            GenSql()
            If (Me.TableType = eTableType.Table) Then
                Return String.Format("INSERT INTO [{0}] ( {1} ) VALUES ( {2} )", Me.Name, Me._sqlUpdateableFields, Me._sqlInsertParamList)
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property SqlUpdate() As String
        Get
            GenSql()
            If (Me.TableType = eTableType.Table) Then
                Return String.Format("UPDATE [{0}] SET {1} ", Me.Name, Me._sqlUpdateCopyList)
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property SqlDelete() As String
        Get
            GenSql()
            If (Me.TableType = eTableType.Table) Then
                Return String.Format("DELETE FROM [{0}] ", Me.Name)
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public Overrides Function ToString() As String
        Return String.Format("TableInfo: {0}", Me.Name)
    End Function

    Protected _sqlAllFields As String
    Protected _sqlUpdateableFields As String
    Protected _sqlInsertParamList As String
    Protected _sqlUpdateCopyList As String
    Protected Sub GenSql()
        If (String.IsNullOrEmpty(Me._sqlAllFields)) Then
            Me._sqlAllFields = ""
            Me._sqlUpdateableFields = ""
            Me._sqlInsertParamList = ""
            Me._sqlUpdateCopyList = ""
            For Each col As ColumnInfo In Me.Columns
                If (Me._sqlAllFields <> "") Then Me._sqlAllFields += ", "
                Me._sqlAllFields += String.Format("[{0}]", col.Name)

                If (Not (col.IsComputed OrElse col.IsIdentity OrElse col.IsRowVersion)) Then
                    If (Me._sqlUpdateableFields <> "") Then Me._sqlUpdateableFields += ", "
                    Me._sqlUpdateableFields += String.Format("[{0}]", col.Name)

                    If (Me._sqlInsertParamList <> "") Then Me._sqlInsertParamList += ", "
                    Me._sqlInsertParamList += String.Format("@{0}", col.Name)

                    If (Me._sqlUpdateCopyList <> "") Then Me._sqlUpdateCopyList += ", "
                    Me._sqlUpdateCopyList += String.Format("[{0}] = @{0}", col.Name)
                End If
            Next
        End If
    End Sub

End Class
