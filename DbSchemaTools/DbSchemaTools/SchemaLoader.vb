﻿Public Class SchemaLoader

    Public Shared Function LoadDbSchema(pConnInfo As Rational.DB.DbConnectionInfo, Optional pErrorHandler As Action(Of Object, SchemaLoaderErrorEventArgs) = Nothing) As DbInfo
        Dim loader As ISchemaLoader
        Select Case pConnInfo.DbType
            Case Rational.DB.eDbType.SqlServer
                loader = New SqlServerSchemaLoader(pConnInfo)
            Case Rational.DB.eDbType.Oracle
                loader = New OracleSchemaLoader(pConnInfo)
            Case Rational.DB.eDbType.MySql
                loader = New MySqlSchemaLoader(pConnInfo)
            Case Else
                loader = New GenericSchemaLoader(pConnInfo)
        End Select
        If (pErrorHandler IsNot Nothing) Then
            AddHandler loader.OnError,
                Sub(s As Object, e As SchemaLoaderErrorEventArgs)
                    pErrorHandler(s, e)
                End Sub
        End If

        'Change this this to debug available schema information
        Dim explore = False
        If (explore) Then ExploreMetadata(pConnInfo)

        Return loader.Load()
    End Function


    Friend Shared Sub ExploreMetadata(pConnInfo As Rational.DB.DbConnectionInfo)
        Dim db As New Rational.DB.Database(pConnInfo)
        Dim fctry = pConnInfo.GetFactory()
        Dim schemaTables = New Dictionary(Of String, DataTable)

        Using conn = fctry.CreateConnection()
            conn.ConnectionString = pConnInfo.ConnectionString
            conn.Open()
            Dim dtMetadataCollections = conn.GetSchema()
            For Each clnDr As DataRow In dtMetadataCollections.Rows
                Dim clnName = clnDr.Item("CollectionName").ToString
                Dim dtCln = conn.GetSchema(clnName)
                schemaTables.Add(clnName, dtCln)
                Debug.Print(String.Format("MetaDataCollection: {0}, Cols: {1}", clnName, String.Join(",", (From c In dtCln.Columns Select DirectCast(c, DataColumn).ColumnName))))
            Next
        End Using
        Debug.Print("")
    End Sub


End Class
