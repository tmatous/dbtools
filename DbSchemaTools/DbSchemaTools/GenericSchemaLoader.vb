﻿Public Class GenericSchemaLoader
    Implements ISchemaLoader

    Protected _connInfo As Rational.DB.DbConnectionInfo

    Public Sub New(pConnInfo As Rational.DB.DbConnectionInfo)
        _connInfo = pConnInfo
    End Sub

    Public Event OnError(sender As Object, e As SchemaLoaderErrorEventArgs) Implements ISchemaLoader.OnError

    Public Function Load() As DbInfo Implements ISchemaLoader.Load
        Dim res As New DbInfo

        Dim dtTables As DataTable
        Dim dtAllCols As DataTable
        Dim db As New Rational.DB.Database(_connInfo)
        Dim fctry = _connInfo.GetFactory()

        'Uncomment this to debug the schema information available
        'SchemaLoader.ExploreMetadata(_connInfo)

        Using conn = fctry.CreateConnection()
            conn.ConnectionString = _connInfo.ConnectionString
            conn.Open()
            dtTables = conn.GetSchema("Tables")
            dtAllCols = conn.GetSchema("Columns")
        End Using
        Dim dtTables_ColNames = (From c As DataColumn In dtTables.Columns.OfType(Of DataColumn)() Select c.ColumnName).ToList
        Dim dtAllCols_ColNames = (From c As DataColumn In dtAllCols.Columns.OfType(Of DataColumn)() Select c.ColumnName).ToList
        Dim possibleTableColNames As IList(Of String) = {"TABLE_NAME", "TABLENAME"}
        Dim possibleSchemaColNames As IList(Of String) = {"TABLE_SCHEM", "TABLESCHEM", "TABLE_SCHEMA", "TABLESCHEMA", "SCHEMA"}
        Dim possibleColumnColNames As IList(Of String) = {"COLUMN_NAME", "COLUMNNAME"}
        Dim possibleDataTypeColNames As IList(Of String) = {"TYPE_NAME", "TYPENAME", "DATA_TYPE"}
        Dim possibleNullableColNames As IList(Of String) = {"NULLABLE", "IS_NULLABLE"}
        Dim possibleSizeColNames As IList(Of String) = {"COLUMN_SIZE", "COLUMNSIZE", "LENGTH", "CHARACTER_MAXIMUM_LENGTH"}

        Dim dtTables_TableColName = (From nm In possibleTableColNames Where dtTables_ColNames.Contains(nm, StringComparer.OrdinalIgnoreCase)).FirstOrDefault
        If (String.IsNullOrEmpty(dtTables_TableColName)) Then Throw New Exception(String.Format("Table name column not found ({0})", String.Join(",", dtTables_ColNames)))

        Dim dtTables_SchemaColName = (From nm In possibleSchemaColNames Where dtTables_ColNames.Contains(nm, StringComparer.OrdinalIgnoreCase)).FirstOrDefault
        If (String.IsNullOrEmpty(dtTables_SchemaColName)) Then dtTables_SchemaColName = Nothing

        Dim dtAllCols_TableColName = (From nm In possibleTableColNames Where dtAllCols_ColNames.Contains(nm, StringComparer.OrdinalIgnoreCase)).FirstOrDefault
        If (String.IsNullOrEmpty(dtAllCols_TableColName)) Then Throw New Exception(String.Format("Table name column not found ({0})", String.Join(",", dtAllCols_ColNames)))

        Dim dtAllCols_ColumnColName = (From nm In possibleColumnColNames Where dtAllCols_ColNames.Contains(nm, StringComparer.OrdinalIgnoreCase)).FirstOrDefault
        If (String.IsNullOrEmpty(dtAllCols_ColumnColName)) Then Throw New Exception(String.Format("Column name column not found ({0})", String.Join(",", dtAllCols_ColNames)))

        Dim dtAllCols_DataTypeColName = (From nm In possibleDataTypeColNames Where dtAllCols_ColNames.Contains(nm, StringComparer.OrdinalIgnoreCase)).FirstOrDefault
        If (String.IsNullOrEmpty(dtAllCols_DataTypeColName)) Then Throw New Exception(String.Format("Data type column not found ({0})", String.Join(",", dtAllCols_ColNames)))

        Dim dtAllCols_NullableColName = (From nm In possibleNullableColNames Where dtAllCols_ColNames.Contains(nm, StringComparer.OrdinalIgnoreCase)).FirstOrDefault
        If (String.IsNullOrEmpty(dtAllCols_NullableColName)) Then Throw New Exception(String.Format("Nullable column not found ({0})", String.Join(",", dtAllCols_ColNames)))

        Dim dtAllCols_SizeColName = (From nm In possibleSizeColNames Where dtAllCols_ColNames.Contains(nm, StringComparer.OrdinalIgnoreCase)).FirstOrDefault
        If (String.IsNullOrEmpty(dtAllCols_SizeColName)) Then Throw New Exception(String.Format("Size column not found ({0})", String.Join(",", dtAllCols_ColNames)))

        For Each tblRow As DataRow In dtTables.Rows
            Dim tbl As New TableInfo
            tbl.Name = tblRow.Item(dtTables_TableColName).ToString
            tbl.ClrName = tbl.Name
            If (dtTables_SchemaColName IsNot Nothing) Then
                tbl.SchemaName = tblRow.Item(dtTables_SchemaColName).ToString
            Else
                tbl.SchemaName = ""
            End If

            For Each colRow As DataRow In dtAllCols.Select(String.Format("{0} = '{1}'", dtAllCols_TableColName, tbl.Name))
                Dim col As New ColumnInfo

                col.Table = tbl
                col.Name = colRow(dtAllCols_ColumnColName).ToString
                col.ClrName = col.Name
                col.Nullable = If((colRow(dtAllCols_NullableColName).ToString.ToUpper = "YES"), True, False)

                col.SqlType = GetDbType(colRow(dtAllCols_DataTypeColName).ToString)
                col.ClrType = ColumnInfo.GetClrType(col.SqlType)
                ColumnInfo.AssignDataClassFromSqlType(col)
                If (col.DataClass = eDataClass.StringType) Then
                    col.StringLength = Convert.ToInt64(colRow(dtAllCols_SizeColName))
                    If (col.StringUnicode) Then col.StringLength = col.StringLength \ 2
                End If

                AssignDdlDataType(col, colRow(dtAllCols_DataTypeColName).ToString)

                col.InPrimaryKey = False
                col.InForeignKey = False
                col.IsIdentity = False
                col.IsRowVersion = False
                col.IsComputed = False

                tbl.Columns.Add(col)
                If (col.InPrimaryKey) Then tbl.PkColumns.Add(col)
                If (col.IsIdentity) Then tbl.IdentityColumn = col
            Next

            res.Tables.Add(tbl)
        Next

        Return res
    End Function

    Protected Function GetDbType(pSqlType As String) As DbType
        pSqlType = pSqlType.ToLower().Trim()
        Select Case pSqlType

            'integer (exact numerics)
            Case "int", "int identity", "autoinc", "integer", "unsigned int", "counter"
                Return DbType.Int32
            Case "smallint", "short"
                Return DbType.Int16
            Case "tinyint", "byte"
                Return DbType.Byte
            Case "bigint", "unsigned bigint"
                Return DbType.Int64

                'boolean (exact numerics)
            Case "bit", "logical"
                Return DbType.Boolean

                'floating point (exact numerics)
            Case "decimal", "numeric"
                Return DbType.Decimal
            Case "money", "smallmoney", "curdouble", "currency"
                Return DbType.Currency

                'floating point (approximate numerics)
            Case "real"
                Return DbType.Single
            Case "float", "double"
                Return DbType.Double

                'strings (non unicode)
            Case "char"
                Return DbType.AnsiStringFixedLength
            Case "varchar", "varchar(max)", "text", "memo", "long varchar", "longchar"
                Return DbType.AnsiString

                'unicode strings
            Case "nchar"
                Return DbType.StringFixedLength
            Case "nvarchar", "nvarchar(max)", "ntext"
                Return DbType.String

                'date and time
            Case "date"
                Return DbType.Date
            Case "time"
                Return DbType.Time
            Case "datetime", "smalldatetime", "datetime2"
                Return DbType.DateTime
            Case "datetimeoffset"
                Return DbType.DateTimeOffset

                'binary
            Case "binary", "varbinary", "varbinary(max)", "image", "blob", "long binary", "longbinary", "bigbinary"
                Return DbType.Binary

                'guid
            Case "guid", "uniqueidentifier"
                Return DbType.Guid

                'rowversion
            Case "rowversion", "timestamp"
                Return DbType.Binary

                'other?
        End Select

        'try some others
        Dim res As DbType? = Nothing
        res = GetDbTypeVFP(pSqlType)
        If (res IsNot Nothing) Then Return res.Value

        'no value here, raise an error event
        RaiseEvent OnError(Me, New SchemaLoaderErrorEventArgs With {.ErrorType = SchemaLoaderErrorEventArgs.eErrorType.UnsupportedType, .Message = "Unsupported type: " + pSqlType})

        Return DbType.Object
    End Function

    'try the type names for FoxPro, these are goofy
    Protected Function GetDbTypeVFP(pSqlType As String) As DbType?
        pSqlType = pSqlType.ToLower().Trim()
        Select Case pSqlType

            'integer (exact numerics)
            Case "n", "i"
                Return DbType.Int32

                'floating point (exact numerics)
            Case "y"
                Return DbType.Currency

                'boolean (exact numerics)
            Case "l"
                Return DbType.Boolean

                'strings (non unicode)
            Case "c"
                Return DbType.AnsiStringFixedLength
            Case "m"
                Return DbType.AnsiString

                'date and time
            Case "d"
                Return DbType.Date
            Case "t"
                Return DbType.Time

                'binary
            Case "g"
                Return DbType.Binary

        End Select

        Return Nothing
    End Function

    ''' <summary>
    ''' Assign the DdlDataType property using the specified SQL type as well as size properties on the column already
    ''' </summary>
    ''' <param name="pioColInfo"></param>
    ''' <param name="pSqlType"></param>
    ''' <remarks></remarks>
    Protected Sub AssignDdlDataType(pioColInfo As ColumnInfo, pSqlType As String)
        Dim typeArgs = ""
        Select Case pSqlType.ToLower
            Case "char", "nchar", "varchar", "nvarchar"
                typeArgs = String.Format("({0})", If(pioColInfo.StringLength = -1, "max", pioColInfo.StringLength.ToString))
            Case "binary", "varbinary"
                'not implemented
                typeArgs = "(?)"
            Case Else
                typeArgs = ""
        End Select

        pioColInfo.DdlDataType = String.Format("{0}{1}", pSqlType, typeArgs)
    End Sub

End Class
