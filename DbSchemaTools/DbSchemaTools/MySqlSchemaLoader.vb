﻿Public Class MySqlSchemaLoader
    Implements ISchemaLoader

    Protected _connInfo As Rational.DB.DbConnectionInfo

    Public Sub New(pConnInfo As Rational.DB.DbConnectionInfo)
        _connInfo = pConnInfo
    End Sub

    Public Event OnError(sender As Object, e As SchemaLoaderErrorEventArgs) Implements ISchemaLoader.OnError

    Public Function Load() As DbInfo Implements ISchemaLoader.Load
        Dim res As New DbInfo

        Dim dtTables As DataTable
        Dim dtAllCols As DataTable
        Dim dtForKeys As DataTable
        Dim db As New Rational.DB.Database(_connInfo)
        Dim fctry = _connInfo.GetFactory()

        Using conn = fctry.CreateConnection()
            conn.ConnectionString = _connInfo.ConnectionString
            conn.Open()
            dtTables = conn.GetSchema("Tables")
            dtAllCols = conn.GetSchema("Columns")

            'load keys
            Dim sqlForKeys = New Rational.DB.DbStatement("SELECT fks.CONSTRAINT_NAME, fks.TABLE_SCHEMA, fks.TABLE_NAME, fkcs.COLUMN_NAME, fkcs.REFERENCED_TABLE_NAME, fkcs.REFERENCED_COLUMN_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS fks INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE fkcs ON fkcs.CONSTRAINT_SCHEMA = fks.CONSTRAINT_SCHEMA AND fkcs.CONSTRAINT_NAME = fks.CONSTRAINT_NAME WHERE fks.CONSTRAINT_TYPE = 'FOREIGN KEY'")
            dtForKeys = db.SelectDataTable(sqlForKeys)
        End Using

        'TODO: load procs and views

        Dim allColsByName = New Dictionary(Of String, ColumnInfo)
        For Each tblRow As DataRow In dtTables.Rows
            Dim tblType = tblRow.Item("TABLE_TYPE").ToString

            Dim tbl As New TableInfo
            If (tblType = "BASE TABLE") Then
                tbl.TableType = TableInfo.eTableType.Table
            ElseIf (tblType = "VIEW") Then
                tbl.TableType = TableInfo.eTableType.View
            Else
                Continue For
            End If
            res.Tables.Add(tbl)

            tbl.Name = tblRow.Item("TABLE_NAME").ToString
            tbl.ClrName = tbl.Name
            tbl.SchemaName = tblRow.Item("TABLE_SCHEMA").ToString


            For Each colRow As DataRow In dtAllCols.Select(String.Format("TABLE_NAME = '{0}' AND TABLE_SCHEMA = '{1}'", tbl.Name, tbl.SchemaName))
                Dim col As New ColumnInfo

                col.Table = tbl
                col.Name = colRow("COLUMN_NAME").ToString
                col.ClrName = col.Name
                col.Nullable = If((colRow("IS_NULLABLE").ToString.ToUpper = "Y"), True, False)

                Dim isUnicode = If((colRow("CHARACTER_SET_NAME").ToString.ToUpper.Contains("UTF")), True, False)
                col.SqlType = GetDbType(colRow("DATA_TYPE").ToString, isUnicode)
                col.ClrType = ColumnInfo.GetClrType(col.SqlType)
                col.ByteLength = 0

                ColumnInfo.AssignDataClassFromSqlType(col)
                If (col.DataClass = eDataClass.StringType) Then
                    col.StringLength = Convert.ToInt64(colRow("CHARACTER_MAXIMUM_LENGTH"))
                End If
                If (col.DataClass = eDataClass.DecimalType) Then
                    col.DecimalPrecision = Convert.ToByte(colRow("NUMERIC_PRECISION"))
                    col.DecimalScale = Convert.ToByte(colRow("NUMERIC_SCALE"))
                End If

                AssignDdlDataType(col, colRow("DATA_TYPE").ToString)

                col.InPrimaryKey = If((colRow("COLUMN_KEY").ToString.ToUpper = "PRI"), True, False)
                col.InForeignKey = False
                col.IsIdentity = If((colRow("EXTRA").ToString.ToUpper = "AUTO_INCREMENT"), True, False)
                col.IsRowVersion = False
                col.IsComputed = False

                tbl.Columns.Add(col)
                If (col.InPrimaryKey) Then tbl.PkColumns.Add(col)
                If (col.IsIdentity) Then tbl.IdentityColumn = col
                allColsByName.Add(String.Format("{0}.{1}.{2}", tbl.SchemaName, tbl.Name, col.Name), col)
            Next
        Next


        'assign FKs
        Dim allFks = New Dictionary(Of String, ForeignKeyInfo)
        For Each keyRow As DataRow In dtForKeys.Rows
            Dim colKey = String.Format("{0}.{1}.{2}", keyRow.Item("TABLE_SCHEMA"), keyRow.Item("TABLE_NAME"), keyRow.Item("COLUMN_NAME"))
            Dim colRefKey = String.Format("{0}.{1}.{2}", keyRow.Item("TABLE_SCHEMA"), keyRow.Item("REFERENCED_TABLE_NAME"), keyRow.Item("REFERENCED_COLUMN_NAME"))
            Dim fkname = keyRow.Item("CONSTRAINT_NAME").ToString
            Dim fk As ForeignKeyInfo
            If (allFks.ContainsKey(fkname)) Then
                fk = allFks.Item(fkname)
            Else
                fk = New ForeignKeyInfo()
                fk.Name = fkname
                allFks.Add(fkname, fk)
            End If
            Dim fkcol = allColsByName(colKey)
            Dim fkcolRef As ColumnInfo = Nothing
            If (allColsByName.ContainsKey(colRefKey)) Then
                fkcolRef = allColsByName.Item(colRefKey)
                If (Not fkcolRef.Table.ReferenceKeys.Contains(fk)) Then fkcolRef.Table.ReferenceKeys.Add(fk)
            End If
            If (Not fkcol.Table.ForeignKeys.Contains(fk)) Then fkcol.Table.ForeignKeys.Add(fk)
            fkcol.InForeignKey = True
            fk.Columns.Add(New ForeignKeyColumnInfo With {.Column = fkcol, .ReferencedColumn = fkcolRef})
        Next

        Return res
    End Function


    'BIT,BLOB,TINYBLOB,MEDIUMBLOB,LONGBLOB,BINARY,VARBINARY,DATE,DATETIME,TIMESTAMP,TIME,CHAR,NCHAR,VARCHAR,NVARCHAR,SET,ENUM,TINYTEXT,TEXT,MEDIUMTEXT,LONGTEXT,DOUBLE,FLOAT,TINYINT,SMALLINT,INT,YEAR,MEDIUMINT,BIGINT,DECIMAL,TINY INT,SMALLINT,MEDIUMINT,INT,BIGINT
    Protected Function GetDbType(pSqlType As String, pIsUnicode As Boolean) As DbType
        pSqlType = pSqlType.ToUpper()
        Select Case pSqlType
            Case "BIT"
                Return DbType.Boolean

            Case "BLOB"
                Return DbType.Binary
            Case "TINYBLOB"
                Return DbType.Binary
            Case "MEDIUMBLOB"
                Return DbType.Binary
            Case "LONGBLOB"
                Return DbType.Binary
            Case "BINARY"
                Return DbType.Binary
            Case "VARBINARY"
                Return DbType.Binary

            Case "DATE"
                Return DbType.Date
            Case "DATETIME"
                Return DbType.DateTime
            Case "TIMESTAMP"
                Return DbType.DateTime
            Case "TIME"
                Return DbType.Time

            Case "CHAR"
                Return If(pIsUnicode, DbType.StringFixedLength, DbType.AnsiStringFixedLength)
            Case "NCHAR"
                Return If(pIsUnicode, DbType.StringFixedLength, DbType.AnsiStringFixedLength)
            Case "VARCHAR"
                Return If(pIsUnicode, DbType.String, DbType.AnsiString)
            Case "NVARCHAR"
                Return If(pIsUnicode, DbType.String, DbType.AnsiString)

            Case "SET"
                Return DbType.AnsiStringFixedLength
            Case "ENUM"
                Return DbType.AnsiStringFixedLength

            Case "TINYTEXT"
                Return If(pIsUnicode, DbType.String, DbType.AnsiString)
            Case "TEXT"
                Return If(pIsUnicode, DbType.String, DbType.AnsiString)
            Case "MEDIUMTEXT"
                Return If(pIsUnicode, DbType.String, DbType.AnsiString)
            Case "LONGTEXT"
                Return If(pIsUnicode, DbType.String, DbType.AnsiString)

            Case "DOUBLE"
                Return DbType.Double
            Case "FLOAT"
                Return DbType.Single

            Case "TINYINT"
                Return DbType.SByte
            Case "SMALLINT"
                Return DbType.Int16
            Case "INT"
                Return DbType.Int32
            Case "YEAR"
                Return DbType.Int16
            Case "MEDIUMINT"
                Return DbType.Int32
            Case "BIGINT"
                Return DbType.Int64

            Case "DECIMAL"
                Return DbType.Decimal

                'these are unsigned, not specified in the type
                'Case "TINY INT"
                '    Return DbType.Byte
                'Case "SMALLINT"
                '    Return DbType.UInt16
                'Case "MEDIUMINT"
                '    Return DbType.UInt32
                'Case "INT"
                '    Return DbType.UInt32
                'Case "BIGINT"
                '    Return DbType.UInt64

        End Select

        'no value here, raise an error event
        RaiseEvent OnError(Me, New SchemaLoaderErrorEventArgs With {.ErrorType = SchemaLoaderErrorEventArgs.eErrorType.UnsupportedType, .Message = "Unsupported type: " + pSqlType})

        Return DbType.Object
    End Function


    ''' <summary>
    ''' Assign the DdlDataType property using the specified SQL type as well as size properties on the column already
    ''' </summary>
    ''' <param name="pioColInfo"></param>
    ''' <param name="pSqlType"></param>
    ''' <remarks></remarks>
    Protected Sub AssignDdlDataType(pioColInfo As ColumnInfo, pSqlType As String)
        Dim typeArgs = ""
        Select Case pSqlType.ToLower
            Case "char", "varchar"
                typeArgs = String.Format("({0})", pioColInfo.StringLength)
            Case "binary", "varbinary"
                typeArgs = String.Format("({0})", pioColInfo.ByteLength)
            Case Else
                typeArgs = ""
        End Select

        pioColInfo.DdlDataType = String.Format("{0}{1}", pSqlType, typeArgs)
    End Sub

End Class
