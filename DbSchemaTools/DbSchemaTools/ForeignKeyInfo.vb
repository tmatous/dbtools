﻿<Serializable>
Public Class ForeignKeyInfo

    Public Property Name As String
    Public Property Columns As IList(Of ForeignKeyColumnInfo) = New List(Of ForeignKeyColumnInfo)

End Class
