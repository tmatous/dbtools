﻿Public Class WaitCursorHandler
    Implements IDisposable

    Private _form As Form
    Private _prevCursor As Cursor

    Public Sub New(pForm As Form)
        _form = pForm

        If (_form IsNot Nothing) Then
            _prevCursor = _form.Cursor
            _form.Cursor = Cursors.WaitCursor
        Else
            _prevCursor = Cursor.Current
            Cursor.Current = Cursors.WaitCursor
        End If
    End Sub

#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                If (_form IsNot Nothing) Then
                    _form.Cursor = _prevCursor
                Else
                    Cursor.Current = _prevCursor
                End If
            End If
        End If
        Me.disposedValue = True
    End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
