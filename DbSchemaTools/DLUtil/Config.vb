﻿Public Class Config

    Public Enum eGeneratorFor
        Database
        Tables
    End Enum

    Public Class Generator
        Public Property GeneratorFor As eGeneratorFor
        Public Property SourceFile As String
        Public Property OutputFilePattern As String
        Public Property CreateOnly As Boolean
    End Class

    Public Property Settings As IDictionary(Of String, String)
    Public Property Generators As IList(Of Generator)

    Public Property DbType As String
    Public Property DbLocation As String
    Public Property CustomizeFile As String
    Public Property WinMergePath As String

    Public Sub New(pFilePath As String)
        LoadConfig(pFilePath)
    End Sub

    Protected Sub LoadConfig(pFilePath As String)
        If (Not IO.File.Exists(pFilePath)) Then Throw New ArgumentException("Config file not found")
        Dim configXml As XDocument = XDocument.Load(pFilePath)
        'If (Not ValidateXml(configXml)) Then Throw New ArgumentException("Invalid Config file")

        Dim configNode As XElement = configXml.Root
        Dim settingsNode As XElement = configNode.Element("Settings")

        Me.Settings = New Dictionary(Of String, String)
        For Each settingNode As XElement In settingsNode.DescendantNodes
            If (settingNode.Name <> "add") Then Throw New NotImplementedException(String.Format("LoadConfig: Unsupported node: {0}", settingNode.Name))

            Me.Settings.Add(settingNode.Attribute("key").Value, settingNode.Attribute("value").Value)
        Next

        Dim generatorsNode As XElement = configNode.Element("Generators")
        Me.Generators = New List(Of Generator)
        For Each generatorNode As XElement In generatorsNode.Elements("Generator")
            Dim gen As New Generator
            Dim forStr = generatorNode.Attribute("for").Value
            If (Not [Enum].TryParse(Of eGeneratorFor)(forStr, gen.GeneratorFor)) Then Throw New Exception(String.Format("LoadConfig: Unable to load generator for: {0}", forStr))
            gen.SourceFile = generatorNode.Attribute("sourceFile").Value
            gen.OutputFilePattern = generatorNode.Attribute("outputFilePattern").Value
            If (generatorNode.Attribute("createOnly") IsNot Nothing) Then
                If (Not Boolean.TryParse(generatorNode.Attribute("createOnly").Value, gen.CreateOnly)) Then Throw New Exception("LoadConfig: Invalid createOnly")
            End If

            Me.Generators.Add(gen)
        Next

        Me.DbType = GetSetting("DB.Type", "")
        Me.DbLocation = GetSetting("DB.Location", "")
        Me.CustomizeFile = GetSetting("DB.CustomizeFile", "")
        Me.WinMergePath = GetSetting("WinMergePath", "")
    End Sub

    Public Function GetSetting(Of T)(pName As String, pDefault As T) As T
        If (Not Settings.ContainsKey(pName)) Then Return pDefault
        Return DirectCast(Convert.ChangeType(Settings.Item(pName), GetType(T)), T)
    End Function

End Class
