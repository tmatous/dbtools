﻿Public Class frmCompare

	Public Sub New(pOriginalText As String, pNewText As String)

		' This call is required by the designer.
		InitializeComponent()

		' Add any initialization after the InitializeComponent() call.
		txtOriginal.Text = pOriginalText
		txtNew.Text = pNewText
	End Sub

	Private Sub frmViewText_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
		txtOriginal.SelectionStart = 0
		txtOriginal.SelectionLength = 0
		txtNew.SelectionStart = 0
		txtNew.SelectionLength = 0
	End Sub

End Class