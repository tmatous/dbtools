﻿Imports DbSchemaTools

Public Class frmMain

    Private _config As Config
    Private _dbInfo As DbSchemaTools.DbInfo

    Private _generatedFiles As List(Of GeneratedFile)

#Region "Init"

    Private Sub frmMain_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

    End Sub

    Sub Init()
        Dim configPath = System.Configuration.ConfigurationManager.AppSettings.Get("configFile")
        If (configPath Is Nothing) Then configPath = "Config.xml"
        _config = New Config(configPath)

        If (_config.DbType = "SqlServer") Then
            Dim loader = New DbSchemaTools.SqlServerSchemaLoader(New Rational.DB.DbConnectionInfo(_config.DbLocation, Rational.DB.eDbType.SqlServer))
            _dbInfo = loader.Load()
        ElseIf (_config.DbType = "Edmx") Then
            Dim loader = New DbSchemaTools.EdmxSchemaLoader(_config.DbLocation)
            _dbInfo = loader.Load()
        Else
            Throw New Exception("Unsupported DB.Type")
        End If

        Dim customizeFile = ResolveRelativeFile(_config.CustomizeFile)
        If (customizeFile IsNot Nothing) Then
            CustomizeTables.ApplyCustomizationFile(_dbInfo.Tables, customizeFile)
        End If

    End Sub

#End Region

#Region "Events"

    Private Sub btnGenerate_Click(sender As System.Object, e As System.EventArgs) Handles btnGenerate.Click
        Using New WaitCursorHandler(Me)
            Init()

            _generatedFiles = New List(Of GeneratedFile)
            For Each gen In _config.Generators
                If (gen.GeneratorFor = Config.eGeneratorFor.Database) Then
                    _generatedFiles.Add(GenerateFileForDatabase(gen.SourceFile, gen.OutputFilePattern, gen.CreateOnly))
                Else
                    _generatedFiles.AddRange(GenerateFileForEachTable(gen.SourceFile, gen.OutputFilePattern, gen.CreateOnly))
                End If
            Next

            lstFiles.Items.Clear()
            For Each fil In _generatedFiles
                Dim add = False
                Dim check = False
                If (ckShowAll.Checked) Then add = True
                If (fil.CreateOnly AndAlso (fil.ChangeState = GeneratedFile.eChangeState.NewFile)) Then add = True : check = True
                If ((Not fil.CreateOnly) AndAlso (fil.ChangeState <> GeneratedFile.eChangeState.Unchanged)) Then add = True : check = True

                If (add) Then
                    Dim itm As New ObjectLabelPair(fil, String.Format("[{0}] {1}", fil.ChangeState, fil.Path))
                    lstFiles.Items.Add(itm)
                    If (check) Then
                        lstFiles.SetItemChecked(lstFiles.Items.Count - 1, True)
                    End If
                End If
            Next
        End Using
    End Sub

    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        Using New WaitCursorHandler(Me)
            For Each itm In lstFiles.CheckedItems
                Dim fil = ObjectLabelPair.GetPayload(Of GeneratedFile)(itm)
                IO.File.WriteAllText(fil.Path, fil.Text)
            Next
        End Using
    End Sub

    Private Sub btnCompare_Click(sender As System.Object, e As System.EventArgs) Handles btnCompare.Click
        If (lstFiles.SelectedItem IsNot Nothing) Then
            Dim fil = ObjectLabelPair.GetPayload(Of GeneratedFile)(lstFiles.SelectedItem)
            Dim origTxt = ""
            If (fil.ChangeState <> GeneratedFile.eChangeState.NewFile) Then origTxt = IO.File.ReadAllText(fil.Path)
            Dim newTxt = fil.Text
            Dim winMergePath = _config.WinMergePath
            If (String.IsNullOrEmpty(winMergePath)) Then winMergePath = "WinMerge\WinMergeU.exe"
            If (Not IO.Path.IsPathRooted(winMergePath)) Then winMergePath = IO.Path.Combine(Application.StartupPath, winMergePath)
            If (IO.File.Exists(winMergePath)) Then
                Dim oldTempFn = GetTempFilename("OldTemp", "txt")
                IO.File.WriteAllText(oldTempFn, origTxt)
                Dim newTempFn = GetTempFilename("NewTemp", "txt")
                IO.File.WriteAllText(newTempFn, newTxt)

                Dim si As New ProcessStartInfo()
                si.FileName = winMergePath
                si.Arguments = "/e /u /wl /wr /maximize /dl ""Current"" /dr ""New"""
                si.Arguments += String.Format(" ""{0}"" ""{1}""", oldTempFn, newTempFn)
                si.UseShellExecute = False
                Process.Start(si)
            Else
                Dim frm As New frmCompare(origTxt, newTxt)
                frm.Show()
            End If
        End If
    End Sub

    Private Sub btnUncheckAll_Click(sender As System.Object, e As System.EventArgs) Handles btnUncheckAll.Click
        For x As Int32 = 0 To lstFiles.Items.Count - 1
            lstFiles.SetItemChecked(x, False)
        Next
    End Sub

#End Region

    Function GetTempFilename(pBase As String, pExt As String) As String
        Return IO.Path.Combine(IO.Path.GetTempPath, String.Format("{0}_{1}.{2}", pBase, Guid.NewGuid, pExt))
    End Function

    Function ResolveRelativeFile(pPath As String) As String
        If (String.IsNullOrEmpty(pPath)) Then Return Nothing
        If (Not IO.Path.IsPathRooted(pPath)) Then pPath = IO.Path.Combine(Application.StartupPath, pPath)
        If (Not IO.File.Exists(pPath)) Then Return Nothing
        Return IO.Path.GetFullPath(pPath)
    End Function

    Function ResolveRelativePath(pPath As String) As String
        If (String.IsNullOrEmpty(pPath)) Then Return Nothing
        If (Not IO.Path.IsPathRooted(pPath)) Then pPath = IO.Path.Combine(Application.StartupPath, pPath)
        If (Not IO.Directory.Exists(pPath)) Then Return Nothing
        Return IO.Path.GetFullPath(pPath)
    End Function

    Function GenerateFileForEachTable(pCodeFile As String, pOutputFileFormat As String, pCreateOnly As Boolean) As List(Of GeneratedFile)
        Dim templateSource = IO.File.ReadAllText(pCodeFile)
        Dim files As New List(Of GeneratedFile)

        For Each tbl As DbSchemaTools.TableInfo In _dbInfo.Tables
            Dim file As New GeneratedFile
            file.Path = String.Format(pOutputFileFormat, tbl.ClrName)
            file.CreateOnly = pCreateOnly

            file.Text = DbCodeGen.CodeGenerator.RenderTemplateForTable(templateSource, tbl)
            files.Add(file)
        Next

        CheckChangeState(files)
        Return files
    End Function

    Function GenerateFileForDatabase(pCodeFile As String, pOutputFileFormat As String, pCreateOnly As Boolean) As GeneratedFile
        Dim templateSource = IO.File.ReadAllText(pCodeFile)
        Dim file As New GeneratedFile
        file.Path = pOutputFileFormat
        file.CreateOnly = pCreateOnly

        file.Text = DbCodeGen.CodeGenerator.RenderTemplateForSchema(templateSource, _dbInfo, _dbInfo.Tables)

        CheckChangeState({file})
        Return file
    End Function

    Sub CheckChangeState(pFiles As IList(Of GeneratedFile))
        For Each file As GeneratedFile In pFiles
            If (IO.File.Exists(file.Path)) Then
                Dim curText As String = IO.File.ReadAllText(file.Path)
                If (curText = file.Text) Then
                    file.ChangeState = GeneratedFile.eChangeState.Unchanged
                Else
                    file.ChangeState = GeneratedFile.eChangeState.Modified
                End If
            Else
                file.ChangeState = GeneratedFile.eChangeState.NewFile
            End If
        Next
    End Sub

    Public Class GeneratedFile

        Public Enum eChangeState
            NotSet
            NewFile
            Modified
            Unchanged
        End Enum

        Public Property ChangeState As eChangeState
        Public Property CreateOnly As Boolean
        Public Property Path As String
        Public Property Text As String
    End Class

End Class
