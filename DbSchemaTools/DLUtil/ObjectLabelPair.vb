﻿Public Class ObjectLabelPair
    Public Property Payload As Object
    Public Property Label As String

    Sub New(pPayload As Object, pLabel As String)
        Payload = pPayload
        Label = pLabel
    End Sub

    Public Overrides Function ToString() As String
        Return Label
    End Function

    Public Shared Function GetPayload(Of TPayloadType)(pOLPair As Object) As TPayloadType
        Return GetPayload(Of TPayloadType)(pOLPair, Nothing)
    End Function

    Public Shared Function GetPayload(Of TPayloadType)(pOLPair As Object, pDefault As TPayloadType) As TPayloadType
        If ((pOLPair Is Nothing) OrElse (Not (TypeOf pOLPair Is ObjectLabelPair))) Then Return pDefault

        Dim olp = DirectCast(pOLPair, ObjectLabelPair)
        If (TypeOf olp.Payload Is TPayloadType) Then
            Return DirectCast(olp.Payload, TPayloadType)
        Else
            Return pDefault
        End If
    End Function

    Public Shared Function FindInList(Of TPayloadType)(pCollection As IEnumerable, pPayloadVal As TPayloadType) As ObjectLabelPair
        For Each curObj As Object In pCollection
            If (TypeOf curObj Is ObjectLabelPair) Then
                Dim val As TPayloadType = GetPayload(Of TPayloadType)(curObj)
                If (val.Equals(pPayloadVal)) Then
                    Return DirectCast(curObj, ObjectLabelPair)
                End If
            End If
        Next
        Return Nothing
    End Function

End Class
