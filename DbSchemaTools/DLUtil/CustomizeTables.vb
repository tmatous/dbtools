﻿Imports System.Xml
Imports System.Xml.Schema
Imports System.Xml.Linq
Imports System.CodeDom.Compiler
Imports DbSchemaTools

Public Class CustomizeTables

    Public Shared Sub ApplyCustomizationFile(pTables As IList(Of TableInfo), pXmlFilepath As String)
        If (Not IO.File.Exists(pXmlFilepath)) Then Throw New ArgumentException("CustomizeTables file not found")
        Dim configXml As XDocument = XDocument.Load(pXmlFilepath)
        If (Not ValidateCustomizeTablesXml(configXml)) Then Throw New ArgumentException("Invalid CustomizeTables file")

        Dim tablesNode As XElement = configXml.Root.Element("Tables")

        Dim tablesDict = pTables.ToDictionary(Function(t) t.Name, StringComparer.OrdinalIgnoreCase)
        Dim removeTables As New List(Of TableInfo)
        If (tablesNode.Attribute("RemoveUnspecified").Value = "Yes") Then
            removeTables.AddRange(pTables)
        End If

        For Each tbl As XElement In tablesNode.Elements("Table")
            Dim tblName As String = tbl.Attribute("PhysicalName").Value
            Dim tblObject As TableInfo = Nothing
            If (Not tablesDict.ContainsKey(tblName)) Then Continue For
            tblObject = tablesDict.Item(tblName)
            If ((tbl.Attribute("Remove") IsNot Nothing) AndAlso (tbl.Attribute("Remove").Value = "Yes")) Then
                If (Not removeTables.Contains(tblObject)) Then removeTables.Add(tblObject)
            Else
                If (removeTables.Contains(tblObject)) Then removeTables.Remove(tblObject)
            End If

            'look for table customizations
            If (tbl.Attribute("LogicalName") IsNot Nothing) Then tblObject.ClrName = tbl.Attribute("LogicalName").Value
            If (tbl.Element("Summary") IsNot Nothing) Then tblObject.Summary = tbl.Element("Summary").Value
            Dim colsElement As XElement = tbl.Element("Columns")
            If (colsElement IsNot Nothing) Then
                For Each colLcv As XElement In colsElement.Elements("Column")
                    Dim col As XElement = colLcv
                    Dim colObject As ColumnInfo = (From c In tblObject.Columns Where c.Name = col.Attribute("PhysicalName").Value).FirstOrDefault
                    If (colObject IsNot Nothing) Then
                        'look for column customizations
                        If (col.Attribute("LogicalName") IsNot Nothing) Then colObject.ClrName = col.Attribute("LogicalName").Value
                        If (col.Element("Summary") IsNot Nothing) Then colObject.Summary = col.Element("Summary").Value
                    End If
                Next
            End If
        Next

        For Each removeTable As TableInfo In removeTables
            If (pTables.Contains(removeTable)) Then pTables.Remove(removeTable)
        Next
    End Sub

    Protected Shared Function ValidateCustomizeTablesXml(pDoc As XDocument) As Boolean
        Dim schemas As New XmlSchemaSet()
        schemas.Add("", XmlReader.Create(Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("DbCodeGen.CustomizeTables_1_1.xsd")))
        Dim xmlOk = True
        pDoc.Validate(schemas, Sub(sender, e)
                                   Debug.Print(e.Message)
                                   xmlOk = False
                               End Sub
                )

        Return xmlOk
    End Function

End Class
