@echo off
set config=Debug
set destdir=Build
echo Ready to build...
pause

set msbuild_exe="C:\Windows\Microsoft.NET\Framework64\v4.0.30319\msbuild.exe"
%msbuild_exe% /t:Build /p:Configuration=%config% DbSchemaTools.sln
if ERRORLEVEL 1 goto Error

md %destdir%
copy DLUtil\bin\%config%\DLUtil.exe %destdir%
copy DLUtil\bin\%config%\*.dll %destdir%
copy DLUtil\Config.sample.xml %destdir%
copy DLUtil\DLUtil.exe.config %destdir%
copy DbSchemaTools\bin\%config%\*.dll %destdir%
copy DbCodeGen\bin\%config%\*.dll %destdir%
copy DbCodeGen\CustomizeTables.sample.xml %destdir%
copy CodeGenTemplates\*.cshtml %destdir%\*.csrzr

echo Done.
pause

goto End
:Error
echo Error!
pause
:End
